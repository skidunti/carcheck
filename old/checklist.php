<?php require_once('header.php'); ?>

        <div class="header">
		
			<?php require_once('menuInicial.php'); ?>
			
		</div>

		<div class="content">
			<div class="wrap">
				
				<?php require_once('sidebar.php'); ?>
				
				<div class="resultado">
					
					<div class="pontosDeAtencao">
						<div class="pontos">
							<div class="total"><p>7.5</p></div>
							<div class="texto">pontos <a href="#">[?]</a></div>							
						</div>
						<div class="descricao">
							<div class="label"><p>Pontos de atenção</p></div>
							<div class="maisInfo">
								<div class="restricoes">
									<div class="ico"></div>
									<div class="dados">
										<span class="g">01</span>
										<span class="p">restricões</span>
									</div>
								</div>
								<div class="impedimentos">
									<div class="ico"></div>
									<div class="dados">
										<span class="g">02</span>
										<span class="p">impedimentos</span>
									</div>								
								</div>
								<div class="alertas">
									<div class="ico"></div>
									<div class="dados">
										<span class="g">01</span>
										<span class="p">alertas</span>
									</div>									
								</div>
							</div>
						</div>
					</div>
					
					<div id="accordion">
						<h3><span class="ico restricao"></span>Restrição judicial</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>
						<h3><span class="ico restricao"></span>Restrição administrativa</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>
						<h3><span class="ico impedimento"></span>Restrição de roubo e furto</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico alerta"></span>Restrição tributária</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Restrição de penhor</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Análise de remarcação de chassi</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Fora de circulação</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Arredondamento mercantil</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Reserva e domínio</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>
						<h3><span class="ico ok"></span>Alienação fiduciária</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Circulação em estado de origem (RJ)</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Chassi original</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Motor original (7127617832627832636328)</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Análise CRLV</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Ocorrência de leilão</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>DPVAT exercício 2013</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Verificação total</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Ocorrência de roubo/furto</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>	
						<h3><span class="ico ok"></span>Decodificação de chassi</h3>
						<div>
							<p>Este espaço pode ser usado para falar um pouco das condições  que caracterizam cada ítem como restrição, impedimento, alerta ou ok.</p>
							<p><a href="#">Saiba mais...</a></p>
						</div>							
					</div>
					
				</div>

			</div>
		</div>
		
		<div class="footer">
			<?php require_once('menuConsultas.php'); ?>
		</div>
		
<?php require_once('footer.php'); ?>
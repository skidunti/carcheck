<?php require_once('header.php'); ?>

        <div class="header">
		
			<?php require_once('menuInicial.php'); ?>
				
			<?php require_once('menuConsultas.php'); ?>
			
		</div>

		<div class="content doisMenus">
			<div class="wrap">
				<div class="inner">
				
					<h1 class="titulo">Consultas realizadas</h1>
					<div class="filtro">
						<p class="rotulo">Filtrar por:</p>
						<div class="botoes">
							<a class="ativo" href="#">Todas</a>
							<a href="#">Consulta completa</a>
							<a href="#">Consulta segura</a>
							<a href="#">Outras consultas</a>
						</div>
						<div class="selects">
						
							<ul class="nivel1">
								<li><a class="selecao" href="#">Por mim<i class="seletor1"></i></a>
									<ul class="nivel2">
										<li>Usuário: <span class="cima"></span>
											<ul class="nivel3">
												<li><a class="lista" href="#">Nome de Usuário<i class="seletor2"></i></a>
													<ul class="nivel4">
														<li><a href="#">Felipe de Castro</a></li>
														<li><a href="#">Leandro Medeiros</a></li>
														<li><a href="#">Joaquim José</a></li>
													</ul>
												</li>
											</ul>
										</li>
										<li><a class="todos" href="#">Todos os usuários</a></li>
									</ul>
								</li>
							</ul>
							
							<ul class="nivel1">
								<li><a class="selecao" href="#">Nos últimos 7 dias<i class="seletor1"></i></a>
									<ul class="nivel2 dias">
										<li><a href="#">Últimos 7 dias</a> <span class="cima"></span></li>
										<li><a href="#">Últimos 15 dias</a></li>
										<li><a href="#">Últimos 30 dias</a></li>
										<li><a class="outroPeriodo" href="#">Outro período</a>
											<ul class="nivel3">
												<li>
													<div class="labelData">início:</div>
													<div class="pegaData"><input type="text" class="calendario" id="datepicker" /></div>
												</li>
												<li>
													<div class="labelData">fim:</div>
													<div class="pegaData"><input type="text" class="calendario" id="datepicker2" /></div>
												</li>
											</ul>
										</li>
									</ul>								
								</li>
							</ul>
							
						</div>
					</div>	

					<div class="conjuntoDeCards">
					
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>

						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>
						
						<div class="card">
							<div class="mascara">
								<a class="detalhes" href="#">
									<div class="nome">VOLKSWAGEN FUSCA</div>
									<div class="dados">1970 / 1971 - LGP 8823</div>
								</a>
							</div>
							<div class="acesso">
								<div class="mais">
									<p class="info">consultado por</p>
									<p class="usuario">Nome do Usuário</p>
									<p class="info">em 00/00/0000 às 00:00</p>
								</div>
								<p class="valor">$0000</p>
							</div>
							<img class="imagem" src="img/carros/fusca.png">
						</div>	

						<a id="next" href="consultas2.php"></a>
						
					</div>
					
				</div>	
			</div>
		</div>
		
<?php require_once('footer.php'); ?>

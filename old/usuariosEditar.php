<?php require_once('header.php'); ?>

        <div class="header">
		
			<?php require_once('menuInicial.php'); ?>
			
		</div>

		<div class="content">
			<div class="wrap">
				<div class="inner">
				
					<h1 class="titulo laranja">Editar cadastro</h1>

					<div class="usuariosEditar">
						<form method="" action="">							
							
								<div class="detalhes info ativo">
									<h3><p class="ico">1</p>Informações pessoais <a class="editar" href="#"><span class="esquerda"></span>editar</a></h3>
									<div class="conteudo">									
										<div class="separador">
											<label>Nome:</label>
											<div class="input"><input name="nome" type="text" value="Felipe de Castro" size="30"></div>
										</div>
										<div class="separador">
											<label>CPF:</label>
											<div class="input"><input name="cpf" type="text" value="8734.738495.364948.00001-903" size="30"></div>
										</div>
										<div class="botoes">
											<input class="proxima" type="button" value="Proxima etapa &nbsp;&nbsp;&nbsp;&nbsp;&rsaquo;">
											<input type="button" value="Cancelar">
										</div>
									</div>
								</div>
								
								<div class="detalhes contatos desativado">
									<h3><p class="ico">2</p>Contatos<a class="editar" href="#"><span class="esquerda"></span>editar</a></h3>
									<div class="conteudo">									
										<div class="separador">
											<label>Telefone 1:</label>
											<div class="input"><input name="telefone1" type="text" value="(21) 2488-8888" size="13" readonly></div>
										</div>
										<div class="separador">
											<label>Telefone 2:</label>
											<div class="input"><input name="telefone2" type="text" value="(21) 2488-8888" size="13" readonly></div>
										</div>
										<div class="botoes">
											<input class="proxima" type="button" value="Proxima etapa  &rsaquo;">
											<input type="button" value="Cancelar">
										</div>
									</div>
								</div>
								
								<div class="detalhes endereco desativado">
									<h3><p class="ico">3</p>Endereço<a class="editar" href="#"><span class="esquerda"></span>editar</a></h3>
									<div class="conteudo">									
										<div class="separador">
											<label>Cep:</label>
											<div class="input"><input name="cep" type="text" value="22795-275" size="9" readonly></div>
										</div>
										<div class="separador">
											<label>Rua/Avenida:</label>
											<div class="input"><input name="rua" type="text" value="Av. Américas" size="45" readonly></div>
										</div>
										<div class="separador">
											<label>Número:</label>
											<div class="input"><input name="numero" type="text" value="290" size="7"  readonly></div>
											<label>Complemento:</label>
											<div class="input"><input name="complemento" type="text" value="apt. 301" size="7" readonly></div>
										</div>
										<div class="separador">
											<label>Bairro:</label>
											<div class="input"><input name="bairro" type="text" value="Recreio" size="31" readonly></div>
										</div>
										<div class="separador">
											<label>Cidade:</label>
											<div class="input"><input name="cidade" type="text" value="Rio de Janeiro" size="31" readonly></div>
										</div>
										<div class="separador">
											<label>UF:</label>
											<div class="input"><input name="uf" type="text" value="RJ" size="2" readonly></div>
										</div>
										<div class="separador">
											<label>Telefone:</label>
											<div class="input"><input name="telefone" type="text" value="(21) 8509-6090" size="13" readonly></div>
										</div>
										<div class="botoes">
											<input class="concluir" type="submit" value="Salvar  &rsaquo;" onclick="javascript:return false;">
											<input type="button" value="Cancelar">
										</div>
									</div>	
								</div>							
							
						</form>
						
						<?php require_once('usuariosMenu.php'); ?>
					
					</div>
					
				</div>	
			</div>
		</div>
		
<?php require_once('footer.php'); ?>
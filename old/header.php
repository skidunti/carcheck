<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="pt-br" class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="pt-br" class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="pt-br" class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="pt-br"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Carcheck</title>
        <meta name="description" content="">
		
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="icon" href="favicon.ico" type="image/x-icon">		

		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">

        <!--[if lt IE 9]>
            <script src="js/vendor/html5shiv.js"></script>
			<script src="js/vendor/PIE.js"></script>
			<script src="js/vendor/selectivizr-min.js"></script>
        <![endif]-->
    </head>
    <body>
	
	<?php require_once('modais.php'); ?>
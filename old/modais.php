<div id="boxes">
	
	<div id="saibaMais" class="window">
		<a href="#" class="close"></a>		
		<h1>Atenção</h1>		
		<p>O tempo para que os créditos sejam adicionados em sua conta pode ser imediato ou demorar até 24hs. Isso acontece devido ao processo de aprovação do pagamento PagSeguro e o tempo que ele demora para nos confirmar que o pagamento foi efetuado corretamente.</p>
		<p>Os créditos adquiridos tem validade de 6 meses a partir do dia da compra. </p>
		<p>O tempo também varia de acordo com a forma de pagamento:</p>
		<p>
			- TEF: A aprovação pode ser imediata ou demorar até 2 hs após a transferência. 
			- Cartão de crédito: A análise e aprovação do pagamento pode ser imediata ou demorar até 24hs. 
			- Boleto Bancário: Confirmação do pagamento é feita 1 dia útil após o pagamento do boleto
		</p>
		<p>Recomendamos que a compra de créditos seja feita com antecedência para não atrapalhar sua tomada de decisão na compra de um veículo.</p>
	</div>
	
	<div id="sucesso" class="window">
		<a href="#" class="close"></a>		
		<h1>Compra efetuada com sucesso</h1>		
		<p>O status da sua compra encontra-se em análise.</p>

		<p>Aguarde a aprovação da compra para a adição dos créditos adquiridos. Você receberá um e-mail assim que sua compra for aprovada.</p>

		<p>A qualquer momento você poderá entrar em contato com nosso suporte através do atendimento online ou enviando um e-mail para suporte@carcheck.com.br</p>
	</div>	
	
	<div id="saldoInsuficiente" class="window">
		<a href="#" class="close"></a>	
		<h1>Saldo insuficiente</h1>
		<p>A consulta que você tentou realizar custa 99 créditos, seu saldo atual é de 88 créditos. Para continuar você precisa comprar mais créditos.</p>

								<div class="detalhes escolha ativo">
									<div class="quant"><input name="quant" type="text" value="" size="4"> créditos</div>
									<div class="conteudo">													
										<div class="separador">
											<div class="quantidade">
												<a href="#"><i></i>5</a>
												<a href="#"><i></i>40</a>
												<a href="#"><i></i>90</a>
												<a href="#" class="outros">Outros
													<div class="OQ"><input name="outrosquant" type="text" value=""></div>
												</a>												
											</div>
											<div class="alerta"><i></i><p>Os créditos podem levar até 24hs para entrar conta.</p> <a href="#saibaMais" name="modal">Saiba mais</a></div>
										</div>
										<div class="separador">
											<div class="tabela">
												<ul class="nivel1">
													<li><div class="desc"><i class="ico"></i><p class="nome">Tabela de preços</p></div>
														<ul class="nivel2">
															<li class="grp">
																<p class="fleft">Consulta completa <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="grp">
																<p class="fleft">Consulta segura <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Consulta nacional <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Consulta importados <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">DPVAT <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Decodificador de chassi <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Gravames <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Leilão <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Multas <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Perda total <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Proprietários <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Roubo e furto <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
														</ul>
													</li>
												</ul>
											</div>
											<div class="formasDePagamento">
												<h2>Formas de pagamento</h2>
												<div class="cartoes">
													<h3>Cartões de crédito (Até 12x)</h3>
													<div class="bandeira visa"></div>
													<div class="bandeira master"></div>
													<div class="bandeira dinners"></div>
													<div class="bandeira aexpress"></div>
													<div class="bandeira elo"></div>
												</div>
												<div class="debito">
													<h3>Débito online / TEF</h3>
													<div class="bandeira bradesco"></div>
													<div class="bandeira itau"></div>
													<div class="bandeira bb"></div>
													<div class="bandeira cube"></div>
													<div class="bandeira hsbc"></div>
												</div>
												<div class="boleto">
													<h3>Boleto e pagamento online</h3>
													<div class="bandeira pboleto"></div>
													<div class="bandeira pagseguro"></div>
												</div>
											</div>
										</div>
										<div class="botoes">
											<input class="proxima" type="button" value="Proxima etapa &nbsp;&nbsp;&nbsp;&nbsp;&rsaquo;">
											<input type="button" value="Cancelar">
										</div>
									</div>
								</div>		
		
	</div>

	<div id="mask"></div>
	
</div>
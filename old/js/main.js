var menuRealizarConsulta = {
	
	tipoDeConsulta: function(){
		$('.consultas .nivel1 > li > a').click(function(){
			$(this).parent().siblings().find('.nivel2').hide();
			$(this).next('.nivel2').toggle();
			if( $(this).hasClass('ativo')==false ){
				$(this).addClass('ativo').parent().siblings().find('a').removeClass('ativo');
			}else{
				$(this).removeClass('ativo');
			}			
			/*$('.nivel2').hide();
			$(this).next('.nivel2').show();
			$(this).addClass('ativo').parent().siblings().find('a').removeClass('ativo');*/
		});
	},
	
	pluginProCheckbox: function(){
		$('input[name=tipoConsulta]').change(function(){
			if($(this).is(':checked')==true){
				$(this).parent().css('background-position','0 -18px');
			}else{
				$(this).parent().css('background-position','0 1px');
			}	
		});
	},	
	
	sanfona: function(){
		$('a.sobre').click(function(){
			$(this).parentsUntil('.escolhas').next('.mais').slideToggle(100);
		});
	}	

}

var filtro = {
	
	selecao: function(){
		$('.selecao').click(function(){
			$(this).parentsUntil('.selects').siblings().find('.nivel2').hide();
			$(this).next('.nivel2').toggle();
			if( $(this).hasClass('ativo')==false ){
				$(this).addClass('ativo').parentsUntil('.selects').siblings().find('a').removeClass('ativo');
			}else{
				$(this).removeClass('ativo');
			}			
		});
		
		$('.filtro .nivel1 .nivel3').click(function(){
			$('.filtro .nivel1 .nivel3 .nivel4').toggle();
		});
		
		$('.cadastro .nivel1').click(function(){
			$('.cadastro .nivel1 .nivel2').toggle();
		});
	},
	
	outroPeriodo: function(){
		$('.outroPeriodo').click(function(){
			$(this).next('.nivel3').show();
		});
	},
	
	novaConsulta:function(){
		$('.nenhumaConsulta .imagem').hover(
			function(){ $(this).find('a').fadeIn(); },
			function(){ $(this).find('a').fadeOut(); }
		);
	}
	
}

var sidebar = {

	nomeDaPagina: function() {
		var loc = window.location.href;
		var output  = loc.split('/').pop().split('.').shift();
		return output;		
	},
	
	selecionaMenu: function(){
	
		$('.menuResumo .palco')
		.addClass( sidebar.nomeDaPagina() )
		.attr('href',sidebar.nomeDaPagina()+'.php')
		.find('span').text(sidebar.nomeDaPagina());
		
		$('.menuResumo .nivel2')
		.find('.'+sidebar.nomeDaPagina())
		.parent('li')
		.remove();
		
	}

}

var checklist = {
	sanfona: function(){
		$( "#accordion" ).accordion({
			collapsible: true
		});
	}
}

var detalhamento = {
	sanfona: function(){
		$( "#detalhamento" ).accordion({
			heightStyle: "content",
			collapsible: true
		});
	},
	scrollpane: function(){
		$('#scrollbar1').tinyscrollbar();
		$('.ui-accordion-header').click(function(){
			$('#scrollbar1').tinyscrollbar();
		});
	},
	posicao:function(){
		$('#detalhamento .posicao').each(function(){
			var alturaParente	= $(this).parent().height();
			var alturaPropria	= $(this).height();
			$(this).css('top', (alturaParente-alturaPropria)/2 );
		});	
		$('.ui-accordion-header').click(function(){
			$('#detalhamento .posicao').each(function(){
				var alturaParente	= $(this).parent().height();
				var alturaPropria	= $(this).height();
				$(this).css('top', (alturaParente-alturaPropria)/2 );
			});
		});
	},
	imgModal:function(){

		$('img.modal').click(function(e){
			e.preventDefault();
			$('body').prepend('<div class="janelaModal"></div>');
			$('.janelaModal').css({
				'display':'block',
				'width':$(window).width(),
				'height':$(document).height()
			});
			$(this).clone().appendTo('.janelaModal');
			$('.janelaModal img').css({
				'top':($(window).height()/2)-$('.janelaModal img').height()/2,
				'left':($(window).width()/2)-$('.janelaModal img').width()/2
			});
			$('.janelaModal').on('click',function(){
				$(this).remove();
			});
		});
	
	}
}

var usuarios = {
	cadastro: function(){
		$('.proxima').click(function(){
			//if( $(this).parents('.detalhes').hasClass('info')==true ){
			//	$(this).parents('.detalhes').find('input').attr('readonly', 'readonly');
			//}else{ $(this).parents('.detalhes').find('input').removeAttr('readonly')}
			$(this).parents('.detalhes').removeClass('ativo').addClass('desativado').find('input').attr('readonly', 'readonly');
			$(this).parents('.detalhes').find('select').attr('disabled','disabled');
			$(this).parents('.detalhes').next('.detalhes').removeClass('desativado').addClass('ativo').find('input').removeAttr('readonly');
			$(this).parents('.detalhes').next('.detalhes').find('select').removeAttr('disabled','disabled');
		});
		$('.detalhes .editar').click(function(){
			//if( $(this).parents('.detalhes').hasClass('info')==false ){
			//	$(this).parents('.detalhes').find('input').attr('readonly', 'readonly');	
			//}else{ $(this).parents('.detalhes').find('input').removeAttr('readonly')}
			$(this).parents('.detalhes').removeClass('desativado').addClass('ativo').find('input').removeAttr('readonly');
			$(this).parents('.detalhes').find('select').removeAttr('disabled','disabled');
			$(this).parents('.detalhes').siblings().removeClass('ativo').addClass('desativado').find('input').attr('readonly', 'readonly');
			$(this).parents('.detalhes').siblings().find('select').attr('disabled','disabled');
			comprarCreditos.mascaras();
		});
	}
}

var comprarCreditos = {
	quantidade:function(){
		$('.quantidade a').click(function(){
			if( $(this).hasClass('outros')==false ){
				$('input[name="quant"]').val( $(this).text() );
				$('.quantidade .outros').animate({'padding-right':18},80).find('.OQ').hide();
			}else{
				$(this).animate({'padding-right':100},80).find('.OQ').show();
				$('input[name="outrosquant"]').blur(function(){
					$('input[name="quant"]').val( $(this).val() );
				});
			}
			$(this).addClass('ativo').siblings().removeClass('ativo');
		});
		$('.quantidade .outros').hover(
			function(){ $(this).animate({'padding-right':100},80).find('.OQ').show(); },
			function(){
				if( $(this).hasClass('ativo')==true ){
					$(this).animate({'padding-right':100},80).find('.OQ').show();
				}else{
					$(this).animate({'padding-right':18},80).find('.OQ').hide();
				}
			}
		);
	},
	pagamento:function(){
		$('.forma a').click(function(){
			$('.pagamento').hide();
			$('.pagamento.'+$(this).attr('class')).show();
			$(this).addClass('ativo').siblings().removeClass('ativo');
		});
	},
	endereco:function(){
		$('input[name="endereco"]').change(function(){
			if( $(this).val()==1 ){
				$('.outroEndereco').show();
			}else{
				$('.outroEndereco').hide();
			}
		});
	},
	banco:function(){
		$('input[name="banco"]').change(function(){
			$(this).parents('.pagamento').find('a.enviar').attr('href',$(this).val());
		});
	},
	mascaras:function(){
		$('input[name="outrosquant"]').mask("9?99999");
		$('input[name="cpf"]').mask("999.999.999-99");
		$('input[name="cep"]').mask("99999-999");
		$('input[name="telefone"] , input[name="telefone1"] , input[name="telefone2"]').mask("(99) 9999-9999?9");
		$('input[name="ncartao"]').mask("999999999999?99999999");
		$('input[name="validade"]').mask("99/9999");
		$('input[name="cvv"]').mask("999");
		$('input[name="nascimento"]').mask("99/99/9999");
		$('input[name="cnpj"]').mask("99.999.999/9999-99");
	},
	select:function(){
		$('select').each(function(){
			$(this).wrap('<div class="wrapSelect"></div>');
			$(this).customSelect();
		});
	}
}

var alertas = {
	saibaMais:function(){
		$('a[name=modal]').click(function(e) {
			e.preventDefault();
			
			var id = $(this).attr('href');
		
			var maskHeight = $(document).height();
			var maskWidth = $(window).width();
		
			$('#mask').css({'width':maskWidth,'height':maskHeight});

			$('#mask').fadeIn(1000);	
			$('#mask').fadeTo("slow",0.8);	
		
			//Get the window height and width
			var winH = $(window).height();
			var winW = $(window).width();
				  
			$(id).css('top',  winH/2-$(id).height()/2);
			$(id).css('left', winW/2-$(id).width()/2);
		
			$(id).fadeIn(2000); 
		
		});
		
		$('.window .close').click(function (e) {
			e.preventDefault();
			
			$('#mask').hide();
			$('.window').hide();
		});		
		
		$('#mask').click(function () {
			$(this).hide();
			$('.window').hide();
		});	
	},
	consultas:function(){
		$('.consultas .fechar').click(function(){
			$(this).parents('.consultas').slideUp(100);
		});
	}
}


$(function() {
		
	menuRealizarConsulta.tipoDeConsulta();
	menuRealizarConsulta.pluginProCheckbox();
	menuRealizarConsulta.sanfona();
	
	filtro.selecao();
	filtro.outroPeriodo();
	filtro.novaConsulta();
	
	sidebar.selecionaMenu();
	
	checklist.sanfona();
	
	detalhamento.sanfona();
	detalhamento.scrollpane();
	detalhamento.posicao();
	detalhamento.imgModal();
	
	usuarios.cadastro();
	
	comprarCreditos.quantidade();
	comprarCreditos.pagamento();
	comprarCreditos.endereco();
	comprarCreditos.banco();
	comprarCreditos.mascaras();
	comprarCreditos.select();
	
	alertas.saibaMais();
	alertas.consultas();
		
	$( "#datepicker , #datepicker2" ).datepicker({
		showOn: "button",
		buttonImage: "img/icones/calendar.png",
		buttonImageOnly: true,
		dateFormat: 'dd/mm/yy',
		dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
		dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
		nextText: 'Próximo',
		prevText: 'Anterior'  
	});

	$('.conjuntoDeCards').infinitescroll({
		navSelector  	: "#next:last",
		nextSelector 	: "a#next:last",
		itemSelector 	: ".conjuntoDeCards .card",
		debug		 	: true,
        maxPage         : undefined,
		loading: {
			finishedMsg	: "<em>Fim da lista</em>",
			img			: "img/loading.gif",
			speed		: 'slow',
			msgText		: ""
		}
    });
	
    if (window.PIE) {
        $('.cadastro, .cadastro .nivel2, .cadastro .nivel2 a, .consultas .nivel1 li > a, .consultas .nivel2, .consultas .nivel2 .custo, .consultas .nivel2 .botoes a, .filtro .botoes a, .filtro .nivel1 .selecao, .filtro .nivel1 .nivel2, .filtro .nivel1 .nivel2, .filtro .nivel1 .nivel3, .filtro .nivel1 .nivel4, .filtro .nivel1 .nivel4 a, .pegaData').each(function() {
            PIE.attach(this);
        });
    }
});
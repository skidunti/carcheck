			<div class="consultas">
				<div class="wrap">
					<div class="texto">
						<h1>Realizar consulta</h1>
						<p>Selecione uma das opções ao lado para visualizar mais informações</p>
					</div>
					<ul class="nivel1">
						<li><a class="completa" href="javascript:void(0);"><i></i>Completa</a>
							<ul class="nivel2">
								<li>
									<span class="cima"></span>
									<h2 class="titulo">Consulta completa</h2>
									<p class="descricao">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt. Proin facilisis laoreet feugiat.</p>
									<p class="adicional">
										Dados necessários:<br />
										Placa, chassi, renavam e CPF
									</p>
									<div class="info">
										<div class="custo">
											<div class="titulo">custo da consulta</div>
											<div class="descricao"><p>35</p></div>
										</div>
										<div class="botoes">
											<a class="consultar" href="#"><i></i>Consultar</a>
											<a class="visualizar" href="#">Visualizar exemplo</a>
										</div>
									</div>
								</li>
							</ul>						
						</li>
						<li><a class="segura" href="javascript:void(0);"><i></i>Segura</a>
							<ul class="nivel2">
								<li>
									<span class="cima"></span>
									<h2 class="titulo">Consulta segura</h2>
									<p class="descricao">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt. Proin facilisis laoreet feugiat.</p>
									<p class="adicional">
										Dados necessários:<br />
										Placa, chassi, renavam e CPF
									</p>
									<div class="info">
										<div class="custo">
											<div class="titulo">custo da consulta</div>
											<div class="descricao"><p>35</p></div>
										</div>
										<div class="botoes">
											<a class="consultar" href="#"><i></i>Consultar</a>
											<a class="visualizar" href="#">Visualizar exemplo</a>
										</div>
									</div>
								</li>
							</ul>						
						</li>
						<li><a class="outras" href="javascript:void(0);"><i></i>Outras<span></span></a>
							<ul class="nivel2">
								<li>
									<span class="cima"></span>
									
									<div class="escolhas">
									
										<div class="tipo">
											<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="consultaNacional"></div>
											<p class="nome">Consulta nacional <a class="sobre" href="#">[?]</a></p>
											<p class="valor">$ 99</p>
										</div>
										<div class="mais">
											<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
											<p class="maisDados">
												Dados necessários:<br />
												Placa, chassi, renavam e CPF
											</p>
										</div>
										
										<div class="tipo">
											<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="consultaImportados"></div>
											<p class="nome">Consulta importados <a class="sobre" href="#">[?]</a></p>
											<p class="valor">$ 99</p>
										</div>
										<div class="mais">
											<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
											<p class="maisDados">
												Dados necessários:<br />
												Placa, chassi, renavam e CPF
											</p>
										</div>
										
										<div class="tipo">
											<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="dpvat"></div>
											<p class="nome">DPVAT <a class="sobre" href="#">[?]</a></p>
											<p class="valor">$ 99</p>
										</div>
										<div class="mais">
											<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
											<p class="maisDados">
												Dados necessários:<br />
												Placa, chassi, renavam e CPF
											</p>
										</div>
										
										<div class="tipo">
											<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="decodificadorDeChassi"></div>
											<p class="nome">Decodificador de chassi <a class="sobre" href="#">[?]</a></p>
											<p class="valor">$ 99</p>
										</div>
										<div class="mais">
											<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
											<p class="maisDados">
												Dados necessários:<br />
												Placa, chassi, renavam e CPF
											</p>
										</div>

										<div class="tipo">
											<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="gravames"></div>
											<p class="nome">Gravames <a class="sobre" href="#">[?]</a></p>
											<p class="valor">$ 99</p>
										</div>
										<div class="mais">
											<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
											<p class="maisDados">
												Dados necessários:<br />
												Placa, chassi, renavam e CPF
											</p>
										</div>	

										<div class="tipo">
											<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="leilao"></div>
											<p class="nome">Leilão <a class="sobre" href="#">[?]</a></p>
											<p class="valor">$ 99</p>
										</div>
										<div class="mais">
											<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
											<p class="maisDados">
												Dados necessários:<br />
												Placa, chassi, renavam e CPF
											</p>
										</div>
										
										<div class="tipo">
											<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="multas"></div>
											<p class="nome">Multas <a class="sobre" href="#">[?]</a></p>
											<p class="valor">$ 99</p>
										</div>
										<div class="mais">
											<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
											<p class="maisDados">
												Dados necessários:<br />
												Placa, chassi, renavam e CPF
											</p>
										</div>
										
										<div class="tipo">
											<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="perdaTotal"></div>
											<p class="nome">Perda total <a class="sobre" href="#">[?]</a></p>
											<p class="valor">$ 99</p>
										</div>
										<div class="mais">
											<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
											<p class="maisDados">
												Dados necessários:<br />
												Placa, chassi, renavam e CPF
											</p>
										</div>
										
										<div class="tipo">
											<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="proprietarios"></div>
											<p class="nome">Proprietários <a class="sobre" href="#">[?]</a></p>
											<p class="valor">$ 99</p>
										</div>
										<div class="mais">
											<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
											<p class="maisDados">
												Dados necessários:<br />
												Placa, chassi, renavam e CPF
											</p>
										</div>
										
									</div>
									
									<div class="info">
										<div class="custo">
											<div class="titulo">custo da consulta</div>
											<div class="descricao"><p>35</p></div>
										</div>
										<div class="botoes">
											<a class="consultar" href="#"><i></i>Consultar</a>
											<a class="visualizar" href="#">Visualizar exemplo</a>
										</div>
									</div>									
								</li>
							</ul>							
						</li>
					</ul>
				</div>
			</div>
<?php require_once('header.php'); ?>

        <div class="header">
		
			<?php require_once('menuInicial.php'); ?>
				
			<?php require_once('menuUsuarios.php'); ?>
			
		</div>

		<div class="content doisMenus">
			<div class="wrap">
				<div class="inner">
				
					<h1 class="titulo">Usuários cadastrados</h1>

					<div class="usuariosLista">

						<div class="usuario admin">
							<i class="ico"></i>
							<p class="nome">Felipe de Castro</p>
							<span class="permissao">(Administrador)</span>
							<a class="editar" href="#">editar</a>
						</div>
						
						<div class="usuario">
							<i class="ico"></i>
							<p class="nome">Felipe de Castro</p>
							<a class="editar" href="#">editar</a>
						</div>	

						<div class="usuario">
							<i class="ico"></i>
							<p class="nome">Felipe de Castro</p>
							<a class="editar" href="#">editar</a>
						</div>	

						<div class="usuario">
							<i class="ico"></i>
							<p class="nome">Felipe de Castro</p>
							<a class="editar" href="#">editar</a>
						</div>							
					
					</div>
					
				</div>	
			</div>
		</div>
		
<?php require_once('footer.php'); ?>
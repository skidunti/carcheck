						<div class="novaConsultaMenu">
							<ul class="nivel1">
								<li><div class="desc"><i class="ico"></i><p class="nome">Créditos</p></div>
									<ul class="nivel2">
										<li>
											<div class="linha titulo">
												<div class="fleft">Saldo atual</div>
												<div class="fright">$90</div>
											</div>
										</li>
										<li>
											<div class="linha titulo">
												<div class="fleft">Custo desta consulta</div>
												<div class="fright">$90</div>
											</div>
											
											<div class="descricaoTitulo">
												<div class="linha">
													<div class="fleft">Consulta nacional</div>
													<div class="fright">-$10</div>
												</div>
												<div class="linha">
													<div class="fleft">Consulta importados</div>
													<div class="fright">-$10</div>
												</div>
												<div class="linha">
													<div class="fleft">Decodificador de chassi</div>
													<div class="fright">-$5</div>
												</div>
												<div class="linha">
													<div class="fleft">DPVAT</div>
													<div class="fright">-$5</div>
												</div>
											</div>
											
										</li>
										<li>
											<div class="linha titulo">
												<div class="fleft">Saldo previsto</div>
												<div class="fright">$60</div>
											</div>
										</li>
									</ul>
								</li>
							</ul>
						</div>
<?php require_once('header.php'); ?>

        <div class="header">
		
			<?php require_once('menuInicial.php'); ?>
			
		</div>

		<div class="content">
			<div class="wrap">
				<div class="inner">
				
					<h1 class="titulo laranja">Comprar créditos</h1>

					<div class="credito">
						<form method="" action="">							
							
								<div class="detalhes escolha ativo">
									<h3><p class="ico">1</p>Escolha um pacote <a class="editar" href="#"><span class="esquerda"></span>editar</a></h3>
									<div class="quant"><input name="quant" type="text" value="" size="4"> créditos</div>
									<div class="conteudo">													
										<div class="separador">
											<div class="detalhe">Cada crédito equivale a R$1</div>
											<div class="quantidade">
												<a href="#"><i></i>5</a>
												<a href="#"><i></i>40</a>
												<a href="#"><i></i>90</a>
												<a href="#" class="outros">Outros
													<div class="OQ"><input name="outrosquant" type="text" value=""></div>
												</a>												
											</div>
											<div class="alerta"><i></i><p>Os créditos podem levar até 24hs para entrar conta.</p> <a href="#saibaMais" name="modal">Saiba mais</a></div>
										</div>
										<div class="separador">
											<div class="tabela">
												<ul class="nivel1">
													<li><div class="desc"><i class="ico"></i><p class="nome">Tabela de preços</p></div>
														<ul class="nivel2">
															<li class="grp">
																<p class="fleft">Consulta completa <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="grp">
																<p class="fleft">Consulta segura <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Consulta nacional <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Consulta importados <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">DPVAT <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Decodificador de chassi <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Gravames <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Leilão <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Multas <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Perda total <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Proprietários <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
															<li class="sgrp">
																<p class="fleft">Roubo e furto <a href="#">[?]</a></p>
																<p class="fright">$ 99</p>
															</li>
														</ul>
													</li>
												</ul>
											</div>
											<div class="formasDePagamento">
												<h2>Formas de pagamento</h2>
												<div class="cartoes">
													<h3>Cartões de crédito (Até 12x)</h3>
													<div class="bandeira visa"></div>
													<div class="bandeira master"></div>
													<div class="bandeira dinners"></div>
													<div class="bandeira aexpress"></div>
													<div class="bandeira elo"></div>
												</div>
												<div class="debito">
													<h3>Débito online / TEF</h3>
													<div class="bandeira bradesco"></div>
													<div class="bandeira itau"></div>
													<div class="bandeira bb"></div>
													<div class="bandeira cube"></div>
													<div class="bandeira hsbc"></div>
												</div>
												<div class="boleto">
													<h3>Boleto e pagamento online</h3>
													<div class="bandeira pboleto"></div>
													<div class="bandeira pagseguro"></div>
												</div>
											</div>
										</div>
										<div class="botoes">
											<input class="proxima" type="button" value="Proxima etapa &nbsp;&nbsp;&nbsp;&nbsp;&rsaquo;">
											<input type="button" value="Cancelar">
										</div>
									</div>
								</div>
								
								<div class="detalhes complementares desativado">
									<h3><p class="ico">2</p>informações complementares<a class="editar" href="#"><span class="esquerda"></span>editar</a></h3>
									<div class="conteudo">
										<div class="separador">
											<div class="descricao">
												<p>Para sua propria segurança, é importante confirmar dados cadastrais junto a bancos, operadoras de cartão de crédito e financeiras.</p>
												<p>Seus dados são sigilosos, armazenados de forma codificada e só você terá acesso a eles.
Além disso, na próxima compra não será mais necessário informar estes dados.</p>
											</div>
										</div>
										<div class="separador">
											<h2>Informe seu CPF</h2>
										</div>
										<div class="separador">
											<label>CPF:</label>
											<div class="input"><input name="cpf" type="text" value="105.840.257-93" size="12" readonly></div>
										</div>
										<div class="separador">
											<h2>Informe seu endereço</h2>
										</div>
										<div class="separador">
											<label>Cep:</label>
											<div class="input"><input name="cep" type="text" value="22795-275" size="9" readonly></div>
										</div>
										<div class="separador">
											<label>Rua/Avenida:</label>
											<div class="input"><input name="rua" type="text" value="Av. Américas" size="40" readonly></div>
										</div>
										<div class="separador">
											<label>Número:</label>
											<div class="input"><input name="numero" type="text" value="290" size="7"  readonly></div>
											<label>Complemento:</label>
											<div class="input"><input name="complemento" type="text" value="apt. 301" size="7" readonly></div>
										</div>
										<div class="separador">
											<label>Bairro:</label>
											<div class="input"><input name="bairro" type="text" value="Recreio" size="31" readonly></div>
										</div>
										<div class="separador">
											<label>Cidade:</label>
											<div class="input"><input name="cidade" type="text" value="Rio de Janeiro" size="31" readonly></div>
										</div>
										<div class="separador">
											<label>UF:</label>
											<div class="input"><input name="uf" type="text" value="RJ" size="2" readonly></div>
										</div>
										<div class="separador">
											<label>Telefone:</label>
											<div class="input"><input name="telefone" type="text" value="(21) 8509-6090" size="13" readonly></div>
										</div>
										<div class="botoes">
											<input class="proxima" type="button" value="Proxima etapa &nbsp;&nbsp;&nbsp;&nbsp;&rsaquo;">
											<input type="button" value="Cancelar">
										</div>
									</div>	
								</div>
								
								<div class="detalhes formaDePagamento desativado">
									<h3><p class="ico">3</p>Forma de Pagamento<a class="editar" href="#"><span class="esquerda"></span>editar</a></h3>
									<div class="conteudo">
										<div class="separador">
											<div class="forma" style="width:680px">
												<a class="cartaoDeCredito" href="javascript:void(0);">Cartão de crédito</a>
												<a class="boleto" href="javascript:void(0);">Boleto</a>
												<a class="depositoOnline" href="javascript:void(0);">Débito online</a>
												<a class="depositoEmConta" href="javascript:void(0);">Depósito em conta</a>
											</div>
										</div>
										
										<!--Cartão de crédito-->
										<div class="pagamento cartaoDeCredito">
											<div class="instrucoes">
												<span class="cima"></span>
												
												<div class="imgCartoes">
													<h3>Cartões de crédito (Até 12x)</h3>
													<div class="bandeira visa"></div>
													<div class="bandeira master"></div>
													<div class="bandeira dinners"></div>
													<div class="bandeira aexpress"></div>
													<div class="bandeira elo"></div>
												</div>
												
												<h2>Dados do cartão</h2>
												<div class="separador">
													<label>Nº do cartão:</label>
													<div class="input"><input name="ncartao" type="text" value="" size="18" readonly></div>
												</div>
												<div class="separador">
													<label>Validade:</label>
													<div class="input"><input name="validade" type="text" value="" size="12" readonly></div>
												</div>
												<div class="separador">
													<label>CVV:</label>
													<div class="input"><input name="cvv" type="text" value="" size="12" readonly></div>
												</div>	
												
												<h2>Dados do dono do cartão</h2>
												<div class="separador">
													<label>CPF:</label>
													<div class="input"><input name="cpf" type="text" value="" size="12" readonly></div>
												</div>
												<div class="separador">
													<label>Telefone:</label>
													<div class="input"><input name="telefone" type="text" value="" size="12" readonly></div>
												</div>
												<div class="separador">
													<label>Nascimento:</label>
													<div class="input"><input name="nascimento" type="text" value="" size="12" readonly></div>
												</div>
												
												<h2>Endereço</h2>
												
												<div class="separador">
													<div class="topico"><input type="radio" name="endereco" value="0"> O mesmo do cadastro?</div>
												</div>
												<div class="separador">
													<div class="topico"><input type="radio" name="endereco" value="1"> Outro endereço</div>
												</div>
												
												<div class="outroEndereco">
													<div class="separador">
														<label>Cep:</label>
														<div class="input"><input name="cep" type="text" value="" size="9" readonly></div>
													</div>
													<div class="separador">
														<label>Rua/Avenida:</label>
														<div class="input"><input name="rua" type="text" value="" size="45" readonly></div>
													</div>
													<div class="separador">
														<label>Número:</label>
														<div class="input"><input name="numero" type="text" value="" size="7"  readonly></div>
														<label>Complemento:</label>
														<div class="input"><input name="complemento" type="text" value="" size="7" readonly></div>
													</div>
													<div class="separador">
														<label>Bairro:</label>
														<div class="input"><input name="bairro" type="text" value="" size="31" readonly></div>
													</div>
													<div class="separador">
														<label>Cidade:</label>
														<div class="input"><input name="cidade" type="text" value="" size="31" readonly></div>
													</div>
													<div class="separador">
														<label>UF:</label>
														<div class="input"><input name="uf" type="text" value="" size="2" readonly></div>
													</div>
													<div class="separador">
														<label>Telefone:</label>
														<div class="input"><input name="telefone" type="text" value="" size="13" readonly></div>
													</div>
												</div>
											</div>
											<div class="botoes">
												<a class="enviar" href="#" target="_blank">Concluir  &rsaquo;</a>
												<input type="button" value="Cancelar">
											</div>											
										</div>
										<!--Fim Cartão de crédito-->
										
										<!--Boleto-->
										<div class="pagamento boleto">
											<div class="instrucoes">
												<span class="cima"></span>
												<div class="alerta"><i></i><p>O pedido será aprovado somente após a aprovação do pagamento.</p></div>
												<h2>Dados pessoais</h2>
												<div class="separador">
													<label>Nome:</label>
													<div class="input"><input name="nome" type="text" value="" size="12" readonly></div>
												</div>
												<div class="separador">
													<label>CPF:</label>
													<div class="input"><input name="cpf" type="text" value="" size="12" readonly></div>
												</div>
												<div class="separador">
													<label>Telefone:</label>
													<div class="input"><input name="uf" type="text" value="" size="12" readonly></div>
												</div>												
											</div>
											<div class="botoes">
												<a class="enviar" href="#" target="_blank">Gerar boleto  &rsaquo;</a>
												<input type="button" value="Cancelar">
											</div>											
										</div>
										<!--Fim Boleto-->
										
										<!--Depósito online-->
										<div class="pagamento depositoOnline">
											<div class="instrucoes">
												<span class="cima"></span>
												<h2>Selecione o banco</h2>
												<table class="bancos">
													<tr>
														<td><img src="img/icones/bancos/itau.jpg"></td>
														<td><img src="img/icones/bancos/hsbc.jpg"></td>
														<td><img src="img/icones/bancos/bradesco.jpg"></td>
														<td><img src="img/icones/bancos/bb.jpg"></td>
													</tr>
													<tr>
														<td><input type="radio" name="banco" value="https://www.itau.com.br/"></td>
														<td><input type="radio" name="banco" value="http://www.hsbc.com.br/"></td>
														<td><input type="radio" name="banco" value="http://www.bradesco.com.br/"></td>
														<td><input type="radio" name="banco" value="http://www.bb.com.br/"></td>
													</tr>
												</table>
											</div>
											<div class="botoes">
												<a class="enviar" href="#" target="_blank">Página do banco  &rsaquo;</a>
												<input type="button" value="Cancelar">
											</div>											
										</div>
										<!--Fim Depósito online-->
										
										<!--Depósito em conta-->
										<div class="pagamento depositoEmConta">
											<div class="instrucoes">
												<span class="cima"></span>
												<h2>Selecione o banco</h2>
												<table class="bancos">
													<tr>
														<td><img src="img/icones/bancos/itau.jpg"></td>
														<td><img src="img/icones/bancos/hsbc.jpg"></td>
														<td><img src="img/icones/bancos/bradesco.jpg"></td>
														<td><img src="img/icones/bancos/bb.jpg"></td>
													</tr>
													<tr>
														<td><input type="radio" name="banco" value="https://www.itau.com.br/"></td>
														<td><input type="radio" name="banco" value="http://www.hsbc.com.br/"></td>
														<td><input type="radio" name="banco" value="http://www.bradesco.com.br/"></td>
														<td><input type="radio" name="banco" value="http://www.bb.com.br/"></td>
													</tr>
												</table>
											</div>
											<div class="botoes">
												<a class="enviar" href="#" target="_blank">Página do banco  &rsaquo;</a>
												<input type="button" value="Cancelar">
											</div>											
										</div>
										<!--Fim Depósito em conta-->
										
										
										<!--
										<div class="separador">
											<label>Cep:</label>
											<div class="input"><input name="cep" type="text" value="22795-275" size="9" readonly></div>
										</div>
										<div class="separador">
											<label>Rua/Avenida:</label>
											<div class="input"><input name="rua" type="text" value="Av. Américas" size="50" readonly></div>
										</div>
										<div class="separador">
											<label>Número:</label>
											<div class="input"><input name="numero" type="text" value="290" size="7"  readonly></div>
											<label>Complemento:</label>
											<div class="input"><input name="complemento" type="text" value="apt. 301" size="7" readonly></div>
										</div>
										<div class="separador">
											<label>Bairro:</label>
											<div class="input"><input name="bairro" type="text" value="Recreio" size="31" readonly></div>
										</div>
										<div class="separador">
											<label>Cidade:</label>
											<div class="input"><input name="cidade" type="text" value="Rio de Janeiro" size="31" readonly></div>
										</div>
										<div class="separador">
											<label>UF:</label>
											<div class="input"><input name="uf" type="text" value="RJ" size="2" readonly></div>
										</div>
										<div class="separador">
											<label>Telefone:</label>
											<div class="input"><input name="telefone" type="text" value="(21) 8509-6090" size="13" readonly></div>
										</div>
										<div class="botoes">
											<input class="concluir" type="submit" value="Salvar  &rsaquo;" onclick="javascript:return false;">
											<input type="button" value="Cancelar">
										</div>
										-->
									</div>	
								</div>							
							
						</form>
						
						<?php require_once('creditosMenu.php'); ?>
					
					</div>
					
				</div>	
			</div>
		</div>
		
<?php require_once('footer.php'); ?>
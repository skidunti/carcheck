<?php require_once('header.php'); ?>

        <div class="header">
		
			<?php require_once('menuInicial.php'); ?>
			
		</div>

		<div class="content">
			<div class="wrap">
				
				<?php require_once('sidebar.php'); ?>
				
				<div class="resultado">

					<div class="linhaDoTempo">
						
						<div class="ano">2013</div>
							
						<div class="data">
							<div class="pin"><div class="inner"></div></div>
							<div class="balao">
								<span class="esquerda"></span>
								<div class="calendario">
									<p class="mes">JAN</p>
									<p class="dia">24</p>
								</div>
								<div class="info">
									<div class="tipo">Compra e venda</div>
									<div class="descricao">
										<p>Concórdia veículos em Recife - PE</p>
									</div>
									<div class="ico carrinho"></div>
								</div>
							</div>
						</div>
						
						<div class="ano">2012</div>
						
						<div class="data">
							<div class="pin"><div class="inner"></div></div>
							<div class="balao">	
								<span class="esquerda"></span>
								<div class="calendario">
									<p class="mes">DEZ</p>
									<p class="dia">4</p>
								</div>
								<div class="info">
									<div class="tipo">DP VAT pago</div>
									<div class="descricao">
										<p>Alguma informação resumida</p>
									</div>
									<div class="ico mais"></div>
								</div>
							</div>
						</div>	

						<div class="data">
							<div class="pin"><div class="inner"></div></div>
							<div class="balao">
								<span class="esquerda"></span>
								<div class="calendario">
									<p class="mes">ABR</p>
									<p class="dia">10</p>
								</div>
								<div class="info">
									<div class="tipo">Veículo roubado</div>
									<div class="descricao">
										<p>Alguma informaçao resumida mas que ocupe mais de uma linha fazendo o balão expandir verticalmente.</p>
									</div>
									<div class="ico mascara"></div>
								</div>
							</div>
						</div>	

						<div class="data">
							<div class="pin"><div class="inner"></div></div>
							<div class="balao">
								<span class="esquerda"></span>
								<div class="calendario">
									<p class="mes">JUL</p>
									<p class="dia">14</p>
								</div>
								<div class="info">
									<div class="tipo">Título do evento</div>
									<div class="descricao">
										<p>Talvez seja possível exibir uma foto para um determinado evento. Com isso em mente deixamos previsto no layout este campo para fotos.</p>
										<img src="img/carros/timeline.jpg">
									</div>
									<div class="ico asterisco"></div>
								</div>
							</div>
						</div>	

						<div class="data">
							<div class="pin"><div class="inner"></div></div>
							<div class="balao">
								<span class="esquerda"></span>
								<div class="calendario">
									<p class="mes">DEZ</p>
									<p class="dia">25</p>
								</div>
								<div class="info">
									<div class="tipo">Título do evento</div>
									<div class="descricao">
										<p>Talvez seja possível exibir uma foto para um determinado evento. Com isso em mente deixamos previsto no layout este campo para fotos.</p>
										<img src="img/carros/timeline.jpg" width="150">
										<img src="img/carros/timeline.jpg" width="150">
										<img src="img/carros/timeline.jpg" width="150">
									</div>
									<div class="ico asterisco"></div>
								</div>
							</div>
						</div>

						<div class="ano">2011</div>
						
						<div class="data">
							<div class="pin"><div class="inner"></div></div>
							<div class="balao">
								<span class="esquerda"></span>
								<div class="calendario">
									<p class="mes">MAR</p>
									<p class="dia">07</p>
								</div>
								<div class="info">
									<div class="tipo">0 Km</div>
									<div class="descricao">
										<p>Recife - PE</p>
									</div>
									<div class="ico asterisco"></div>
								</div>
							</div>
						</div>
						
					</div>
				
				</div>

			</div>
		</div>
		
		<div class="footer">
			<?php require_once('menuConsultas.php'); ?>
		</div>
		
<?php require_once('footer.php'); ?>
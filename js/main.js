var menuRealizarConsulta = {
	
	tipoDeConsulta: function(){
		$('.consultas .nivel1 > li > a').click(function(e){
			e.preventDefault();
			$(this).parent().siblings().find('.nivel2').hide();
			$(this).next('.nivel2').toggle();
			if( $(this).hasClass('ativo')==false ){
				$(this).addClass('ativo').parent().siblings().find('a').removeClass('ativo');
			}else{
				$(this).removeClass('ativo');
			}			
			/*$('.nivel2').hide();
			$(this).next('.nivel2').show();
			$(this).addClass('ativo').parent().siblings().find('a').removeClass('ativo');*/
			
			if( $(this).hasClass('outras')==false ){
				//zera os checkboxes em Outras, pois outro tipo de consulta foi selecionado
				$('.consultas .nivel1 li.last-child input[type="checkbox"]').attr('checked',false).parent().css('background-position','0 1px');
				//desabilita a consulta
				$('.consultas .nivel1 li.last-child .consultar').addClass('disabled');				
			}else{
				//se fechou e abriu o botao Outras também zera as checkbox e desabilita a consulta
				if( $(this).hasClass('ativo') ){				
					//zera os checkboxes em Outras
					$('.consultas .nivel1 li.last-child input[type="checkbox"]').attr('checked',false).parent().css('background-position','0 1px');
					//desabilita o proximo
					$('.consultas .nivel1 li.last-child .consultar').addClass('disabled');					
				}
			}
			
		});
		
		//last-child, pros IE's entenderem
		$('.consultas .nivel1 > li:last-child').addClass('last-child');
		
		//se selecionar pelo menos um checkbox, o botao consultar é habilitado
		$('.consultas .nivel1 li.last-child input[type="checkbox"]').click(function(){
			if($('.consultas .nivel1 li.last-child input[type="checkbox"]:checked').length > 0){
				//habilita o proximo
				$('.consultas .nivel1 li.last-child .consultar').removeClass('disabled');
			}else{
				//desabilita o proximo
				$('.consultas .nivel1 li.last-child .consultar').addClass('disabled');
			}
		});
	},
	
	pluginProCheckbox: function(){
		$('input[name=tipoConsulta]').change(function(){
			if($(this).is(':checked')==true){
				$(this).parent().css('background-position','0 -18px');
			}else{
				$(this).parent().css('background-position','0 1px');
			}	
		});
	},	
	
	sanfona: function(){
		$('a.sobre').click(function(e){
			e.preventDefault();
			$(this).parentsUntil('.escolhas').next('.mais').slideToggle(100);
		});
	}	

}

var filtro = {
	
	selecao: function(){
		$('.selecao').click(function(e){
			e.preventDefault();
			$(this).parentsUntil('.selects').siblings().find('.nivel2').hide();
			$(this).next('.nivel2').toggle();
			if( $(this).hasClass('ativo')==false ){
				$(this).addClass('ativo').parentsUntil('.selects').siblings().find('a').removeClass('ativo');
			}else{
				$(this).removeClass('ativo');
			}			
		});
		
		$('.filtro .nivel1 .nivel3').click(function(e){
			e.preventDefault();
			$('.filtro .nivel1 .nivel3 .nivel4').toggle();
		});
		
		$('.cadastro .nivel1').click(function(e){
			e.preventDefault();
			$('.cadastro .nivel1 .nivel2').toggle();
		});
	},
	
	outroPeriodo: function(){
		$('.outroPeriodo').click(function(e){
			e.preventDefault();
			$(this).next('.nivel3').show();
		});
	},
	
	novaConsulta:function(){
		$('.nenhumaConsulta .imagem').hover(
			function(){ $(this).find('a').fadeIn(); },
			function(){ $(this).find('a').fadeOut(); }
		);
	}
	
}

var consultas = {
	cardHover: function(){
		$( ".conjuntoDeCards .card" ).hover(
			function(){
				$(this).addClass('hover')
			},
			function(){
				$(this).removeClass('hover')
			}
		);
	}
}

var sidebar = {

	nomeDaPagina: function() {
		var loc = window.location.href;
		var output  = loc.split('/').pop().split('.').shift();
		return output;		
	},
	
	selecionaMenu: function(){
	
		$('.menuResumo .palco')
		.addClass( sidebar.nomeDaPagina() )
		.attr('href',sidebar.nomeDaPagina()+'.php')
		.find('span').text(sidebar.nomeDaPagina());
		
		$('.menuResumo .nivel2')
		.find('.'+sidebar.nomeDaPagina())
		.parent('li')
		.remove();
		
	}

}

var checklist = {
	sanfona: function(){
		$( "#accordion" ).accordion({
			collapsible: true
		});
	}
}

var detalhamento = {
	sanfona: function(){
		$( "#detalhamento" ).accordion({
			heightStyle: "content",
			collapsible: true
		});
	},
	scrollpane: function(){
		$('#scrollbar1').tinyscrollbar();
		$('.ui-accordion-header').click(function(e){
			e.preventDefault();
			$('#scrollbar1').tinyscrollbar();
		});
	},
	posicao:function(){
		$('#detalhamento .posicao').each(function(){
			var alturaParente	= $(this).parent().height();
			var alturaPropria	= $(this).height();
			$(this).css('top', (alturaParente-alturaPropria)/2 );
		});	
		$('.ui-accordion-header').click(function(e){
			e.preventDefault();
			$('#detalhamento .posicao').each(function(){
				var alturaParente	= $(this).parent().height();
				var alturaPropria	= $(this).height();
				$(this).css('top', (alturaParente-alturaPropria)/2 );
			});
		});
	},
	imgModal:function(){

		$('img.modal').click(function(e){
			e.preventDefault();
			$('body').prepend('<div class="janelaModal"></div>');
			$('.janelaModal').css({
				'display':'block',
				'width':$(window).width(),
				'height':$(document).height()
			});
			$(this).clone().appendTo('.janelaModal');
			$('.janelaModal img').css({
				'top':($(window).height()/2)-$('.janelaModal img').height()/2,
				'left':($(window).width()/2)-$('.janelaModal img').width()/2
			});
			$('.janelaModal').on('click',function(e){
				e.preventDefault();
				$(this).remove();
			});
		});
	
	}
}

var usuarios = {
	cadastro: function(){
		$('.proxima').click(function(e){
			e.preventDefault();
			//if( $(this).parents('.detalhes').hasClass('info')==true ){
			//	$(this).parents('.detalhes').find('input').attr('readonly', 'readonly');
			//}else{ $(this).parents('.detalhes').find('input').removeAttr('readonly')}
			$(this).parents('.detalhes').removeClass('ativo').addClass('desativado').find('input').attr('readonly', 'readonly');
			$(this).parents('.detalhes').find('select').attr('disabled','disabled');
			$(this).parents('.detalhes').next('.detalhes').removeClass('desativado').addClass('ativo').find('input').removeAttr('readonly');
			$(this).parents('.detalhes').next('.detalhes').find('select').removeAttr('disabled','disabled');
		});
		$('.detalhes .editar').click(function(e){
			e.preventDefault();
			//if( $(this).parents('.detalhes').hasClass('info')==false ){
			//	$(this).parents('.detalhes').find('input').attr('readonly', 'readonly');	
			//}else{ $(this).parents('.detalhes').find('input').removeAttr('readonly')}
			$(this).parents('.detalhes').removeClass('desativado').addClass('ativo').find('input').removeAttr('readonly');
			$(this).parents('.detalhes').find('select').removeAttr('disabled','disabled');
			$(this).parents('.detalhes').siblings().removeClass('ativo').addClass('desativado').find('input').attr('readonly', 'readonly');
			$(this).parents('.detalhes').siblings().find('select').attr('disabled','disabled');
			comprarCreditos.mascaras();
		});
	}
}

var comprarCreditos = {
	quantidade:function(){
		$('.quantidade a').click(function(e){
			e.preventDefault();
			if( $(this).hasClass('outros')==false ){
				$('input[name="quant"]').val( $(this).text() );
				$('.quantidade .outros').animate({'padding-right':18},80).find('.OQ').hide();
			}else{
				$(this).animate({'padding-right':100},80).find('.OQ').show();
				$('input[name="outrosquant"]').blur(function(){
					$('input[name="quant"]').val( $(this).val() );
				});
			}
			$(this).addClass('ativo').siblings().removeClass('ativo');
		});
		$('.quantidade .outros').hover(
			function(){ $(this).animate({'padding-right':100},80).find('.OQ').show(); },
			function(){
				if( $(this).hasClass('ativo')==true ){
					$(this).animate({'padding-right':100},80).find('.OQ').show();
				}else{
					$(this).animate({'padding-right':18},80).find('.OQ').hide();
				}
			}
		);
	},
	pagamento:function(){
		$('.forma a').click(function(e){
			e.preventDefault();
			$('.pagamento').hide();
			$('.pagamento.'+$(this).attr('class')).show();
			$(this).addClass('ativo').siblings().removeClass('ativo');
		});
	},
	endereco:function(){
		$('input[name="endereco"]').change(function(){
			if( $(this).val()==1 ){
				$('.outroEndereco').show();
			}else{
				$('.outroEndereco').hide();
			}
		});
	},
	banco:function(){
		$('input[name="banco"]').change(function(){
			$(this).parents('.pagamento').find('a.enviar').attr('href',$(this).val());
		});
	},
	mascaras:function(){
		$('input[name="outrosquant"]').mask("9?99999");
		$('input[name="cpf"]').mask("999.999.999-99");
		$('input[name="cep"]').mask("99999-999");
		$('input[name="telefone"] , input[name="telefone1"] , input[name="telefone2"]').mask("(99) 9999-9999?9");
		$('input[name="ncartao"]').mask("999999999999?99999999");
		$('input[name="validade"]').mask("99/9999");
		$('input[name="cvv"]').mask("999");
		$('input[name="nascimento"]').mask("99/99/9999");
		$('input[name="cnpj"]').mask("99.999.999/9999-99");
	},
	select:function(){
		$('select').each(function(){
			$(this).wrap('<div class="wrapSelect"></div>');
			$(this).customSelect();
		});
	}
}

var novaConsulta = {
	tipoDeConsulta: function(){
		$('.pagNovaConsulta .escolha input[type="button"]:disabled.proxima').addClass('disabled');
		$('.nivel1.novaConsulta > li > a').click(function(e){
			e.preventDefault();

			//o botao proximo é habilitado quando os botoes de consulta estão ativos, exceto quando é o botão outros por conta do checkbox
			$('.pagNovaConsulta .escolha .proxima').removeClass('disabled').removeAttr('disabled','disabled');
			
			$(this).parent().siblings().find('.nivel2').hide();
			$(this).next('.nivel2').toggle();
			
			if( $(this).hasClass('ativo')==false ){				
				$(this).addClass('ativo').parent().siblings().find('a').removeClass('ativo');			
			}else{
				$(this).removeClass('ativo');
				//o botao proximo é desabilitado quando os botoes de consulta estão desativados
				$('.pagNovaConsulta .escolha .proxima').addClass('disabled').attr('disabled','disabled');				
			}
			
			if($(this).hasClass('outras')){
				//espaçamento
				$(this).parents('.nivel1').css('margin-bottom','335px');
				
				//interacao
				if( $(this).hasClass('ativo')==false ){
					//espaçamento
					$(this).parents('.nivel1').css('margin-bottom','10px');				
				}
				//o botao proximo é desabilitado até clicar num checkbox
				$('.pagNovaConsulta .escolha .proxima').addClass('disabled').attr('disabled','disabled');

				//se fechou e abriu o botao Outras também zera as checkbox e desabilita o proximo
				if( $(this).hasClass('ativo') ){				
					//zera os checkboxes em Outras
					$('.novaConsulta input[type="checkbox"]').attr('checked',false).parent().css('background-position','0 1px');					
				}
				
				//altera o resumo para a proxima etapa quando clica no checkbox, mais abaixo
				
			}else{
				//espaçamento
				$(this).parents('.nivel1').css('margin-bottom','230px');

				//interacao
				if( $(this).hasClass('ativo')==false ){
					//espaçamento
					$(this).parents('.nivel1').css('margin-bottom','10px');					
				}

				//zera os checkboxes em Outras, pois outro tipo de consulta foi selecionado
				$('.novaConsulta input[type="checkbox"]').attr('checked',false).parent().css('background-position','0 1px');
				
				//altera o resumo para a proxima etapa
				if($(this).hasClass('completa')){
					$('.camada').text('Consulta completa');
				}else if($(this).hasClass('segura')){
					$('.camada').text('Consulta segura');
				}
			}			
		});
		
		//se selecionar pelo menos um checkbox, o botao próximo é habilitado
		$('.novaConsulta input[type="checkbox"]').click(function(){
			if($('.novaConsulta input[type="checkbox"]:checked').length > 0){
				//habilita o proximo
				$('.pagNovaConsulta .escolha .proxima').removeClass('disabled').removeAttr('disabled','disabled');
			}else{
				//desabilita o proximo
				$('.pagNovaConsulta .escolha .proxima').addClass('disabled').attr('disabled','disabled');
			}
			
			//altera o resumo para a proxima etapa
			var selected = [];
			$('.novaConsulta input[type="checkbox"]:checked').each(function() {
				   selected.push($(this).attr('title'));
			  });			
			
			$('.camada').text(selected);
			
		});
		
		//habilita o consultar quando preenche a placa ou o chassi
		$('.pagNovaConsulta .complementares .placa , .pagNovaConsulta .complementares .chassi').keypress(function(){
			$('.pagNovaConsulta .complementares .proxima').removeAttr('disabled','disabled');
		});
		
	},
	alterna:function(){
		$('input[name="escolha"]').change(function(e){
			e.preventDefault();
			$(this).parents('.separador').siblings().find('.alterna').hide();
			$(this).parents('.separador').find('.alterna').show();
			$('.alterna input[type="text"]').val('');
			$('.pagNovaConsulta .complementares .proxima').attr('disabled','disabled');
		});
		$('.pagNovaConsulta .editar').click(function(e){
			e.preventDefault();
			$(this).parents('.detalhes').find('.conteudo').show();
			if( $(this).parents('.detalhes').hasClass('escolha')==false ){
				$('.camada').show();
				$('.pagNovaConsulta .complementares .conteudo').hide();
			}else{
				$('.camada').hide();
			}
		});
		$('.detalhes.escolha.ativo .proxima').click(function(){
			$('.camada').show();
		});
	}
}

var alertas = {
	saibaMais:function(){
		$('a[name=modal]').click(function(e) {
			e.preventDefault();
			
			var id = $(this).attr('href');
		
			var maskHeight = $(document).height();
			var maskWidth = $(window).width();
		
			$('#mask').css({'width':maskWidth,'height':maskHeight});

			$('#mask').fadeIn(1000);	
			$('#mask').fadeTo("slow",0.8);	
		
			//Get the window height and width
			var winH = $(window).height();
			var winW = $(window).width();
				  
			$(id).css('top',  winH/2-$(id).height()/2);
			$(id).css('left', winW/2-$(id).width()/2);
		
			$(id).fadeIn(2000); 
		
		});
		
		$('.window .close').click(function (e) {
			e.preventDefault();
			
			$('#mask').hide();
			$('.window').hide();
		});		
		
		$('#mask').click(function () {
			$(this).hide();
			$('.window').hide();
		});	
	},
	consultas:function(){
		$('.consultas .fechar').click(function(){
			$(this).parents('.consultas').slideUp(100);
		});
	}
}

var calendario = {
	data:function(){
		$( "#datepicker , #datepicker2" ).datepicker({
			showOn: "button",
			buttonImage: "img/icones/calendar.png",
			buttonImageOnly: true,
			dateFormat: 'dd/mm/yy',
			dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
			dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
			dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
			monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
			monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
			nextText: 'Próximo',
			prevText: 'Anterior'  
		});	
	}
}

var paginacao = {
	consulta:function(){
		$('.conjuntoDeCards').infinitescroll({
			navSelector  	: "#next:last",
			nextSelector 	: "a#next:last",
			itemSelector 	: ".conjuntoDeCards .card",
			debug		: true,
			maxPage         : undefined,
			loading: {
				finishedMsg	: "<em>Fim da lista</em>",
				img		: "img/loading.gif",
				speed		: 'slow',
				msgText		: ""
			}
		},
		function(){
			consultas.cardHover();
		});		
	}
}

$(function() {

	paginacao.consulta();
	calendario.data();
		
	menuRealizarConsulta.tipoDeConsulta();
	menuRealizarConsulta.pluginProCheckbox();
	menuRealizarConsulta.sanfona();

	consultas.cardHover();
	
	filtro.selecao();
	filtro.outroPeriodo();
	filtro.novaConsulta();
	
	sidebar.selecionaMenu();
	
	checklist.sanfona();
	
	detalhamento.sanfona();
	detalhamento.scrollpane();
	detalhamento.posicao();
	detalhamento.imgModal();
	
	usuarios.cadastro();
	
	comprarCreditos.quantidade();
	comprarCreditos.pagamento();
	comprarCreditos.endereco();
	comprarCreditos.banco();
	comprarCreditos.mascaras();
	comprarCreditos.select();
	
	novaConsulta.tipoDeConsulta();
	novaConsulta.alterna();
	
	alertas.saibaMais();
	alertas.consultas();
	
});
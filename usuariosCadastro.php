<?php require_once('header.php'); ?>

        <div class="header">
		
			<?php require_once('menuInicial.php'); ?>
			
		</div>

		<div class="content">
			<div class="wrap">
				<div class="inner">
				
					<h1 class="titulo laranja">Cadastrar novo usuário</h1>

					<div class="usuariosCadastro">
						<form method="" action="">							
							
								<div class="detalhes info ativo">
									<h3><p class="ico">1</p>Informações pessoais <a class="editar" href="#"><span class="esquerda"></span>editar</a></h3>
									<div class="conteudo">									
										<div class="separador">
											<label>Nome:</label>
											<div class="input"><input name="nome" type="text" value="Felipe de Castro" size="30"></div>
										</div>
										<div class="separador">
											<label>E-mail:</label>
											<div class="input"><input name="email" type="text" value="felipe@skidun.com.br" size="30"></div>
										</div>
										<div class="botoes">
											<input class="proxima" type="button" value="Proxima etapa &nbsp;&nbsp;&nbsp;&nbsp;&rsaquo;">
											<input type="button" value="Cancelar">
										</div>
									</div>
								</div>
								
								<div class="detalhes senha desativado">
									<h3><p class="ico">2</p>Senha<a class="editar" href="#"><span class="esquerda"></span>editar</a></h3>
									<div class="conteudo">									
										<div class="separador">
											<label>Digite a senha:</label>
											<div class="input"><input name="senha" type="password" value="senha" size="17"></div>
										</div>
										<div class="separador">
											<label>Repita a senha:</label>
											<div class="input"><input name="senhaRepete" type="password" value="senha" size="17"></div>
										</div>
										<div class="botoes">
											<input class="proxima" type="button" value="Proxima etapa  &rsaquo;">
											<input type="button" value="Cancelar">
										</div>
									</div>
								</div>
								
								<div class="detalhes privilegios desativado">
									<h3><p class="ico">3</p>Privilégios<a class="editar" href="#"><span class="esquerda"></span>editar</a></h3>
									<div class="conteudo">									
										<div class="separador">
											<label>Nível de acesso</label>
											<select name="acesso">
												<option value="Administrador">Administrador</option>
												<option value="usuário">Usuário</option>
											</select>
										</div>
										<div class="separador">
											<label>Status</label>
											<select name="status">
												<option value="ativo">Ativo</option>
												<option value="inativo">Inativo</option>
											</select>
										</div>
										<div class="botoes">
											<input class="concluir" type="submit" value="Concluir  &rsaquo;" onclick="javascript:return false;">
											<input type="button" value="Cancelar">
										</div>
									</div>	
								</div>							
							
						</form>
						
						<?php require_once('usuariosMenu.php'); ?>
					
					</div>
					
				</div>	
			</div>
		</div>
		
<?php require_once('footer.php'); ?>
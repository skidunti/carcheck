<?php require_once('header.php'); ?>

        <div class="header">
		
			<?php require_once('menuInicial.php'); ?>
			
		</div>

		<div class="content">
			<div class="wrap">
				
				<?php require_once('sidebar.php'); ?>
				
				<div class="resultado">
					
					<div class="historico">
						<div class="nome">
							<p>histórico de rodagem</p>
						</div>
						<div class="descricao">
							<div class="detalhes">
								<div class="quadro">
									<span class="numero">
										<p>4</p>
										<span class="direita"></span>
									</span>
									<span class="texto"><p>anos</p></span>
								</div>
								<div class="quadro">
									<span class="numero">
										<p>2</p>
										<span class="direita"></span>
									</span>
									<span class="texto"><p>proprietários</p></span>
								</div>
							</div>
							<div class="origem">
								<div class="pin"></div>
								<div class="relacao">
									<p class="onde">Origem:</p>
									<p class="cidadeOrigem">Rio de Janeiro</p>
									<p class="passagem">Já passou por:</p>
									<p class="cidadePassagem">Rio de Janeiro - RJ, São Paulo - SP</p>
									<p class="cidadePassagem">Juiz de Fora - MG</p>
								</div>
							</div>
							<div class="quilometragem">
								<div class="separador"><p>Quilometragem</p></div>
								<div class="tabela">
									<div class="informada">
										<p class="t">Informada:</p>
										<p class="g">30.000</p>
									</div>
									<div class="estimada">
										<p class="t">Estimada:</p>
										<p class="g">60.000</p>
									</div>										
									<div class="diferenca">
										<span class="esquerda"></span>
										<p class="g">-50%</p>
										<p class="t">de diferença</p>
									</div>
								</div>
							</div>							
						</div>
					</div>
					
					<div class="estrutural">
						<div class="referencia">
							<img class="imagem" src="img/icones/estrutural.png">
							<div class="texto">RESUMO ESTRUTURAL</div>
						</div>
						
						<div class="lista">						
						
							<div class="linha">
								<p class="item">Já foi roubado</p>
								
								<div class="status nao"><span>não</span></div>
							</div>
							
							<div class="linha">
								<p class="item">Chassi remarcado</p>
								
								<div class="status nao"><span>não</span></div>
							</div>

							<div class="linha">
								<p class="item">DPVAT</p>
								
								<div class="status"><span>Pagamento contínuo</span></div>
							</div>	

							<div class="linha">
								<p class="item">Sinistros</p>
								
								<div class="status"><span>Nenhum</span></div>
							</div>		

							<div class="linha">
								<p class="item">Leilões</p>
								
								<div class="status"><span>Nenhum</span></div>
							</div>
							
							<div class="linha">
								<p class="item">Motor</p>
								
								<div class="status"><span>Original</span></div>
							</div>	

							<div class="linha">
								<p class="item">Apropriado p/ seguradora</p>
								
								<div class="status nao"><span>não</span></div>
							</div>	

							<div class="linha">
								<p class="item">Recall</p>
								
								<div class="status sim"><span>sim</span></div>
							</div>	

							<div class="linha">
								<p class="item">Decodificação do chassi</p>
								
								<div class="status"><span>Ok</span></div>
							</div>							
							
						</div>
					</div>
					
					<div class="legal">
						<div class="referencia">
							<img class="imagem" src="img/icones/legal.png">
							<div class="texto">RESUMO legal</div>
						</div>
						<div class="lista">
						
							<div class="linha">
								<p class="item">Alienações</p>								
								<div class="status nao"><span>não</span></div>
							</div>
							
							<div class="linha">
								<p class="item">Situação</p>								
								<div class="status"><span>Em circulação</span></div>
							</div>

							<div class="linha">
								<p class="item">Financiamento</p>								
								<div class="status"><span>Em aberto</span></div>
							</div>	

							<div class="linha">
								<p class="item">DPVAT deste ano</p>								
								<div class="status"><span>Pago</span></div>
							</div>		

							<div class="linha">
								<p class="item">Renajud</p>								
								<div class="status"><span>Não há registro</span></div>
							</div>
							
							<div class="linha">
								<p class="item">Duplicidade doe motor</p>								
								<div class="status nao"><span>não</span></div>
							</div>	

							<div class="linha">
								<p class="item">Dívidas</p>								
								<div class="status nao"><span>não</span></div>
							</div>	
						
						</div>
					</div>
					
				</div>

			</div>
		</div>
		
		<div class="footer">
			<?php require_once('menuConsultas.php'); ?>
		</div>
		
<?php require_once('footer.php'); ?>
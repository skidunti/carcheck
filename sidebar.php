				<div class="sidebar">
				
					<div class="card">
						<div class="mascara">
							<div class="detalhes">
								<div class="dados">Fiat Adventure 1.8 16V Flex 2008/2008</div>
							</div>
						</div>
						<img class="imagem" src="img/carros/adventure.jpg">
					</div>
					
					<div class="menuResumo">						
						<a class="palco" href="#"><i></i><span></span></a>
						<ul class="nivel2">
							<li><a class="resumo" href="resumo.php"><i></i><span>Resumo</span></a></li>
							<li><a class="checklist" href="checklist.php"><i></i><span>Checklist</span></a></li>
							<li><a class="timeline" href="timeline.php"><i></i><span>Timeline</span></a></li>
							<li><a class="detalhamento" href="detalhamento.php"><i></i><span>Detalhamento</span></a></li>
						</ul>
					</div>
					
					<div class="dadosVeiculo">
						<h1 class="titulo">Dados do veículo</h1>
						<div class="linha">
							<div class="nome">Placa:</div>
							<div class="descricao">KVB 9460</div>
						</div>
						<div class="linha">
							<div class="nome">Chassi:</div>
							<div class="descricao">93ybsr29kaj319966</div>
						</div>
						<div class="linha">
							<div class="nome">Renavam:</div>
							<div class="descricao">169712516</div>
						</div>
						<div class="linha">
							<div class="nome">Motor:</div>
							<div class="descricao">k4md694q020145</div>
						</div>
						<div class="linha">
							<div class="nome">Ano modelo:</div>
							<div class="descricao">2008</div>
						</div>
						<div class="linha">
							<div class="nome">Ano fab:</div>
							<div class="descricao">2008</div>
						</div>
						<div class="linha">
							<div class="nome">Cor:</div>
							<div class="descricao">Preta</div>
						</div>
						<div class="linha">
							<div class="nome">Combustível:</div>
							<div class="descricao">Alcool/Gasolina</div>
						</div>
						<div class="linha">
							<div class="nome">Cilindradas:</div>
							<div class="descricao">1598</div>
						</div>
						<div class="linha">
							<div class="nome">Estado:</div>
							<div class="descricao">RJ</div>
						</div>
						<div class="linha">
							<div class="nome">Cidade:</div>
							<div class="descricao">Rio de Janeiro</div>
						</div>
						<div class="linha">
							<div class="nome">Situação:</div>
							<div class="descricao">Em circulação</div>
						</div>
					</div>
					
					<div class="dadosConsulta">
						<h1 class="titulo">Dados da consulta</h1>
						<div class="linha">
							<div class="nome">Consultor:</div>
							<div class="descricao">FELIPE DE CASTRO</div>
						</div>
						<div class="linha">
							<div class="nome">Data:</div>
							<div class="descricao">29/06/2013</div>
						</div>
						<div class="linha">
							<div class="nome">Código:</div>
							<div class="descricao">130717-195530-64939-169791</div>
						</div>
					</div>
					
				</div>
<?php require_once('header.php'); ?>

        <div class="header">
		
			<?php require_once('menuInicial.php'); ?>
			
		</div>

		<div class="content">
			<div class="wrap">
				<div class="inner">
				
					<h1 class="titulo laranja">Realizar nova consulta</h1>

					<div class="credito pagNovaConsulta">
						<form method="" action="">							
							
								<div class="detalhes escolha ativo">
									<h3><p class="ico">1</p>Tipo de consulta <a class="editar" href="#"><span class="esquerda"></span>editar</a></h3>
									
									<div class="camada" style="display:none">Consulta nacional, importado, decodificador de chassi e DPVAT</div>
									
									<div class="conteudo">													

									<ul class="nivel1 novaConsulta">
										<li><a class="completa" href="javascript:void(0);"><i></i>Completa</a>
											<ul class="nivel2">
												<li>
													<div class="info">
														<div class="custo">
															<div class="titulo">custo da consulta</div>
															<div class="descricao"><p>35</p></div>
														</div>
														<div class="botoes">
															<!--<a class="consultar" href="#"><i></i>Consultar</a>-->
															<a class="visualizar" href="#">Visualizar exemplo</a>
														</div>
													</div>												
													<span class="cima"></span>
													<h2 class="titulo">Consulta completa</h2>
													<p class="descricao">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt. Proin facilisis laoreet feugiat.</p>
													<p class="adicional">
														Dados necessários:<br />
														Placa, chassi, renavam e CPF
													</p>
												</li>
											</ul>						
										</li>
										<li><a class="segura" href="javascript:void(0);"><i></i>Segura</a>
											<ul class="nivel2">
												<li>
													<div class="info">
														<div class="custo">
															<div class="titulo">custo da consulta</div>
															<div class="descricao"><p>35</p></div>
														</div>
														<div class="botoes">
															<a class="visualizar" href="#">Visualizar exemplo</a>
														</div>
													</div>												
													<span class="cima"></span>
													<h2 class="titulo">Consulta segura</h2>
													<p class="descricao">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt. Proin facilisis laoreet feugiat.</p>
													<p class="adicional">
														Dados necessários:<br />
														Placa, chassi, renavam e CPF
													</p>
												</li>
											</ul>						
										</li>
										<li><a class="outras" href="javascript:void(0);"><i></i>Outras<span></span></a>
											<ul class="nivel2">
												<li>
												
													<div class="info">
														<div class="custo">
															<div class="titulo">custo da consulta</div>
															<div class="descricao"><p>35</p></div>
														</div>
														<div class="botoes">
															<a class="visualizar" href="#">Visualizar exemplo</a>
														</div>
													</div>
													
													<span class="cima"></span>
													
													<div class="escolhas">
													
														<div class="tipo">
															<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="consultaNacional" title="Consulta nacional"></div>
															<p class="nome">Consulta nacional <a class="sobre" href="#">[?]</a></p>
															<p class="valor">$ 99</p>
														</div>
														<div class="mais">
															<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
															<p class="maisDados">
																Dados necessários:<br />
																Placa, chassi, renavam e CPF
															</p>
														</div>
														
														<div class="tipo">
															<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="consultaImportados" title="Consulta importados"></div>
															<p class="nome">Consulta importados <a class="sobre" href="#">[?]</a></p>
															<p class="valor">$ 99</p>
														</div>
														<div class="mais">
															<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
															<p class="maisDados">
																Dados necessários:<br />
																Placa, chassi, renavam e CPF
															</p>
														</div>
														
														<div class="tipo">
															<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="dpvat" title="DPVAT"></div>
															<p class="nome">DPVAT <a class="sobre" href="#">[?]</a></p>
															<p class="valor">$ 99</p>
														</div>
														<div class="mais">
															<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
															<p class="maisDados">
																Dados necessários:<br />
																Placa, chassi, renavam e CPF
															</p>
														</div>
														
														<div class="tipo">
															<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="decodificadorDeChassi" title="Decodificador de chassi"></div>
															<p class="nome">Decodificador de chassi <a class="sobre" href="#">[?]</a></p>
															<p class="valor">$ 99</p>
														</div>
														<div class="mais">
															<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
															<p class="maisDados">
																Dados necessários:<br />
																Placa, chassi, renavam e CPF
															</p>
														</div>

														<div class="tipo">
															<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="gravames" title="Gravames"></div>
															<p class="nome">Gravames <a class="sobre" href="#">[?]</a></p>
															<p class="valor">$ 99</p>
														</div>
														<div class="mais">
															<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
															<p class="maisDados">
																Dados necessários:<br />
																Placa, chassi, renavam e CPF
															</p>
														</div>	

														<div class="tipo">
															<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="leilao" title="Leilão"></div>
															<p class="nome">Leilão <a class="sobre" href="#">[?]</a></p>
															<p class="valor">$ 99</p>
														</div>
														<div class="mais">
															<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
															<p class="maisDados">
																Dados necessários:<br />
																Placa, chassi, renavam e CPF
															</p>
														</div>
														
														<div class="tipo">
															<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="multas" title="Multas"></div>
															<p class="nome">Multas <a class="sobre" href="#">[?]</a></p>
															<p class="valor">$ 99</p>
														</div>
														<div class="mais">
															<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
															<p class="maisDados">
																Dados necessários:<br />
																Placa, chassi, renavam e CPF
															</p>
														</div>
														
														<div class="tipo">
															<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="perdaTotal" title="Perda total"></div>
															<p class="nome">Perda total <a class="sobre" href="#">[?]</a></p>
															<p class="valor">$ 99</p>
														</div>
														<div class="mais">
															<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
															<p class="maisDados">
																Dados necessários:<br />
																Placa, chassi, renavam e CPF
															</p>
														</div>
														
														<div class="tipo">
															<div class="bgCheckbox"><input type="checkbox" name="tipoConsulta" value="proprietarios" title="Proprietários"></div>
															<p class="nome">Proprietários <a class="sobre" href="#">[?]</a></p>
															<p class="valor">$ 99</p>
														</div>
														<div class="mais">
															<p class="resumo">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed arcu sem, porta ut accumsan in, aliquam in tortor. Nullam mattis vestibulum tincidunt.</p>
															<p class="maisDados">
																Dados necessários:<br />
																Placa, chassi, renavam e CPF
															</p>
														</div>
														
													</div>										
									
												</li>
											</ul>							
										</li>
									</ul>									

										<div class="botoes">
											<input class="proxima" type="button" value="Proxima etapa &nbsp;&nbsp;&nbsp;&nbsp;&rsaquo;" disabled="disabled">
											<input type="button" value="Cancelar">
										</div>
									</div>
								</div>
								
								<div class="detalhes complementares desativado">
									<h3><p class="ico">2</p>informações complementares<a class="editar" href="#" style="display:none"><span class="esquerda"></span>editar</a></h3>
									<div class="conteudo" style="display:none">
										<div class="separador">
											<h2>Para este(s) tipo(s) de consulta é necessário preencher a placa ou chassi.<br />Selecione uma das opções abaixo.</h2>
										</div>
										<div class="separador placa">
											<label>Placa <input type="radio" name="escolha" value="placa" checked="checked"></label>
											<div class="alterna">
												<div class="input"><input name="placa" type="text" value="" size="12"></div>
												<img src="img/simbolos/placa.png" class="identificador">
											</div>
										</div>
										<div class="separador chassi">
											<label>Chassi <input type="radio" name="escolha" value="placa"></label>
											<div class="alterna" style="display:none">
												<div class="input"><input name="chassi" type="text" value="" size="12"></div>
												<img src="img/simbolos/chassi.png" class="identificador">
											</div>
										</div>
										<div class="separador">
											<h2>O ítem abaixo é de preenchimento opcional</h2>
										</div>
										<div class="separador">
											<label>Quilometragem</label>
											<div class="input"><input name="quilometragem" type="text" value="" size="12"></div>
											<img src="img/simbolos/quilometragem.png" class="identificador quilometragem">
										</div>
										
										<div class="botoes">
											<input class="proxima" type="button" value="Consultar &nbsp;&nbsp;&rsaquo;" disabled="disabled">
											<input type="button" value="Cancelar">
										</div>
									</div>	
								</div>
								
	
						</form>
						
						<?php require_once('novaConsultaMenu.php'); ?>
					
					</div>
					
				</div>	
			</div>
		</div>
		
<?php require_once('footer.php'); ?>
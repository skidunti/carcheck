<?php require_once('header.php'); ?>

        <div class="header">
		
			<?php require_once('menuInicial.php'); ?>
			
		</div><!--header-->

		<div class="content">
			<div class="wrap">
				
				<?php require_once('sidebar.php'); ?>
				
				<div class="resultado">

					<div id="detalhamento">
						
						<h3>
							<span class="ico carNac"></span>
							<span class="nome">Características e dados nacionais</span>
						</h3>
						<div>
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/martelo.jpg"/>
										<p class="legenda">licensiamento</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">Marca / Modelo</div>
										<div class="fright">volkswagen - gol</div>
									</div>
									<div>
										<div class="fleft">Ano de fabricação</div>
										<div class="fright">2006</div>
									</div>
									<div>
										<div class="fleft">Ano do modelo</div>
										<div class="fright">2006</div>
									</div>
									<div>
										<div class="fleft">Munic&Iacute;pio / UF</div>
										<div class="fright">uberlândia - mg</div>
									</div>
									<div>
										<div class="fleft">Combustível</div>
										<div class="fright">gasolina</div>
									</div>
									<div>
										<div class="fleft">Cor</div>
										<div class="fright">preto</div>
									</div>
									<div>
										<div class="fleft">Esp&Eacute;cie</div>
										<div class="fright">particular</div>
									</div>
									<div>
										<div class="fleft">Procedência</div>
										<div class="fright">nacional</div>
									</div>
									<div>
										<div class="fleft">Número de passageiros</div>
										<div class="fright">5</div>
									</div>
									<div>
										<div class="fleft">Categoria</div>
										<div class="fright">particular</div>
									</div>										
								</div>		
							</div>
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/bola.jpg"/>
										<p class="legenda">Técnicos</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">Tipo de montagem</div>
										<div class="fright">completa</div>
									</div>
									<div>
										<div class="fleft">Número do motor</div>
										<div class="fright">789456123457</div>
									</div>
									<div>
										<div class="fleft">Número cx do câmbio</div>
										<div class="fright">487845</div>
									</div>
									<div>
										<div class="fleft">Tipo de veículo</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">Tipo de carroceria</div>
										<div class="fright">eixo simples</div>
									</div>
									<div>
										<div class="fleft">Potência</div>
										<div class="fright">101</div>
									</div>
									<div>
										<div class="fleft">Cilindradas</div>
										<div class="fright">1000</div>
									</div>								
								</div>		
							</div>	
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/carga.jpg"/>
										<p class="legenda">Carga</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">Nº da carroceria</div>
										<div class="fright">778844</div>
									</div>
									<div>
										<div class="fleft">Quantidade de eixos</div>
										<div class="fright">2</div>
									</div>
									<div>
										<div class="fleft">Nº do eixo traseiro</div>
										<div class="fright">4</div>
									</div>
									<div>
										<div class="fleft">Nº do eixo auxiliar</div>
										<div class="fright">4</div>
									</div>
									<div>
										<div class="fleft">Cap. máx. de tração</div>
										<div class="fright">100</div>
									</div>
									<div>
										<div class="fleft">Peso bruto total</div>
										<div class="fright">1000</div>
									</div>
									<div>
										<div class="fleft">Capacidade de carga</div>
										<div class="fright">200</div>
									</div>								
								</div>		
							</div>
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/carga.jpg"/>
										<p class="legenda">dados de compra<br />(faturamento)</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">CNPJ</div>
										<div class="fright">923892838393</div>
									</div>
									<div>
										<div class="fleft">Razão social</div>
										<div class="fright">carros do zé ltda</div>
									</div>
									<div>
										<div class="fleft">Endereço / bairro</div>
										<div class="fright">rua alberto simões, 877 - são pedro</div>
									</div>
									<div>
										<div class="fleft">Cidade / estado</div>
										<div class="fright">são paulo - sp</div>
									</div>
									<div>
										<div class="fleft">Cep</div>
										<div class="fright">26.087-876</div>
									</div>
									<div>
										<div class="fleft">Telefone</div>
										<div class="fright">(011) 8374-8374</div>
									</div>							
								</div>		
							</div>
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/importacao.jpg"/>
										<p class="legenda">Importação</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">CNPJ do importador</div>
										<div class="fright">983298327920</div>
									</div>
									<div>
										<div class="fleft">Código do orgão srf</div>
										<div class="fright">78</div>
									</div>
									<div>
										<div class="fleft">Número reda</div>
										<div class="fright">389485</div>
									</div>
									<div>
										<div class="fleft">Número di</div>
										<div class="fright">211</div>
									</div>
									<div>
										<div class="fleft">Data de registro di</div>
										<div class="fright">12/08/2008</div>
									</div>
									<div>
										<div class="fleft">Tipo de operação de importação</div>
										<div class="fright">1</div>
									</div>
									<div>
										<div class="fleft">Número de processo de importação</div>
										<div class="fright">47789</div>
									</div>	
								</div>		
							</div>	
						</div><!--Características e dados nacionais-->
						
						<h3>
							<span class="ico carEst"></span>
							<span class="nome">Características e dados estaduais</span>
						</h3>
						<div>
							<div>
								<div class="holder">
									<div class="img pin"><p class="nome">MG</p></div>
									<div class="detalhes">
										<div>
											<div class="fleft">Data de licenciamento</div>
											<div class="fright">06/02/2013</div>
										</div>
										<div>
											<div class="fleft">Situação licenciamento</div>
											<div class="fright">---</div>
										</div>
										<div>
											<div class="fleft">Placa anterior</div>
											<div class="fright">xm9711</div>
										</div>
										<div>
											<div class="fleft">Município anterior</div>
											<div class="fright">uberlândia</div>
										</div>									
									</div>
								</div>
								<table>
									<thead>
										<tr>
											<td>RESTRIÇÃO</td>
											<td>DESCRIÇÃO</td>
										</tr>
										<tr>
											<th>RESTRIÇÃO ESTADUAL</th>
											<th>endereço desatualizado</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>RESTRIÇÃO ESTADUAL</td>
											<td>restrições sem registro</td>
										</tr>
										<tr>
											<td colspan="2" class="nenhum">nenhum débito estadual foi encontrado</td>
										</tr>
									</tbody>
								</table>
								<table>
									<thead>
										<tr>
											<td width="39">multa</td>
											<td width="80">ORGÃO</td>
											<td width="143">descrição</td>
											<td width="121">local</td>
											<td width="36">valor</td>
											<td>situação</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>---</td>
											<td>pref. de mg uberlândia</td>
											<td>transitar pela contramão de direção em via c sinalização regul sentido único</td>
											<td>06/05/2013 17:20 rua natal oliveira marques, 26 uberlândia</td>
											<td>191,54</td>
											<td>a pagar</td>
										</tr>
									</tbody>
								</table>
								<table>
									<thead>
										<tr>
											<td width="39">multa</td>
											<td width="80">ORGÃO</td>
											<td width="143">descrição</td>
											<td width="121">local</td>
											<td width="36">valor</td>
											<td>situação</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>---</td>
											<td>pref. de go catalão</td>
											<td>transitar em velocidade superior a máxima permitida em até 20</td>
											<td>23/06/2012 07:10 rua wagner e. campos 247 n. 24 catalão</td>
											<td>---</td>
											<td>notificação da autuação emitida</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div><!--Características e dados estaduais-->
						
						<h3>
							<span class="ico tabelaFipe"></span>
							<span class="nome">Tabela FIPE</span>
						</h3>
						<div>
							<table>
								<thead>
									<tr>
										<td>modelo</td>
										<td>fabricante</td>
										<td>ano</td>
										<td>combustível</td>
										<td>preço fipe</td>
										<td>preço médio</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>golf flash 1.6 mi / 1.6 mi tot. flex 8v 4p;</td>
										<td>vw - volkswagen</td>
										<td>6</td>
										<td>G</td>
										<td>27.238,00</td>
										<td>27.238,00</td>
									</tr>
								</tbody>
							</table>							
						</div><!--Tabela FIPE-->
						
						<h3>
							<span class="ico precoDeMercado"></span>
							<span class="nome">Preço de mercado</span>
						</h3>
						<div>
							<table>
								<thead>
									<tr>
										<td>volkswagen / golf flash 1.6 total flex</td>
										<td>estado (pr)</td>
										<td>brasil</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>preço mínimo</td>
										<td>r$ 37.500,00</td>
										<td>r$ 37.500,00</td>
									</tr>
									<tr>
										<td>preço médio</td>
										<td>r$ 37.500,00</td>
										<td>r$ 37.500,00</td>
									</tr>
									<tr>
										<td>preço máximo</td>
										<td>r$ 37.500,00</td>
										<td>r$ 37.500,00</td>
									</tr>
								</tbody>
							</table>							
						</div><!--Preço de mercado-->

						<h3>
							<span class="ico icoHistorico"></span>
							<span class="nome">Histórico de roubo e furto</span>
						</h3>
						<div>
							<table>
								<tbody>
									<tr class="destaque">
										<td>b.o.</td>
										<td>ano</td>
										<td>municipio</td>
										<td>cód. órgão de segurança</td>
									</tr>								
									<tr>
										<td>000003/1995</td>
										<td>2012</td>
										<td>rio de janeiro</td>
										<td>003</td>
									</tr>
									<tr class="destaque">
										<td>recuperado</td>
										<td>ano</td>
										<td>município</td>
										<td>cód. órgão de segurança</td>
									</tr>
									<tr>
										<td>sim</td>
										<td>2013</td>
										<td>duque de caxias</td>
										<td>003</td>
									</tr>
									<tr class="destaque">
										<td>devolvido</td>
										<td>ano</td>
										<td>município</td>
										<td>cód. órgão de segurança</td>
									</tr>
									<tr>
										<td>sim</td>
										<td>2013</td>
										<td>duque de caxias</td>
										<td>003</td>
									</tr>
								</tbody>
							</table>							
						</div><!--Histórico de roubo e furto-->	

						<h3>
							<span class="ico icoMultas"></span>
							<span class="nome">Multas na polícia rodoviária federal</span>
						</h3>
						<div>
							<table>
								<thead>
									<tr>
										<td>infração</td>
										<td>placa</td>
										<td>uf</td>
										<td>data da infração</td>
										<td>valor</td>
									</tr>								
								</thead>
								<tbody>					
									<tr>
										<td>Entregar veíc pessoa s/ adaptaçõesimpostas concessão/renovação licença conduzir</td>
										<td>jcb4795</td>
										<td>RJ</td>
										<td>10/06/2012</td>
										<td>R$ 300,00</td>
									</tr>
									<tr>
										<td>Entregar veíc pessoa s/ adaptaçõesimpostas concessão/renovação licença conduzir</td>
										<td>jcb4795</td>
										<td>RJ</td>
										<td>10/06/2012</td>
										<td>R$ 300,00</td>
									</tr>
									<tr>
										<td>Entregar veíc pessoa s/ adaptaçõesimpostas concessão/renovação licença conduzir</td>
										<td>jcb4795</td>
										<td>RJ</td>
										<td>10/06/2012</td>
										<td>R$ 300,00</td>
									</tr>
									<tr>
										<td>Entregar veíc pessoa s/ adaptaçõesimpostas concessão/renovação licença conduzir</td>
										<td>jcb4795</td>
										<td>RJ</td>
										<td>10/06/2012</td>
										<td>R$ 300,00</td>
									</tr>
									<tr>
										<td>Entregar veíc pessoa s/ adaptaçõesimpostas concessão/renovação licença conduzir</td>
										<td>jcb4795</td>
										<td>RJ</td>
										<td>10/06/2012</td>
										<td>R$ 300,00</td>
									</tr>
								</tbody>
							</table>							
						</div><!--Multas na polícia rodoviária federal-->
						
						<h3>
							<span class="ico icoGravames"></span>
							<span class="nome">Gravames</span>
						</h3>
						<div>
							<div class="detalhes" style="width:100%">
								<div class="holder">
									<div class="titGravames"><span>veículo financiado</span></div>
									<div class="fright">
										<div class="alerta"><span>Registro</span> incluído em 15/12/2012 (5 meses)</div>
										<div class="alerta"><span>STATUS</span> ALIENAÇÃO FIDUCIÁRIA BAIXADO PELA FINANCEIRA</div>
									</div>
								</div>
								<div>
									<div class="fleft">financiado em nome de</div>
									<div class="fright">fabiana pereira dos santos</div>
								</div>
								<div>
									<div class="fleft">cpf / cnpj</div>
									<div class="fright">8347849379953930</div>
								</div>
								<div>
									<div class="fleft">endereço / bairro</div>
									<div class="fright">giuseppe dichiara 268 d pedro ii - Jardim Petrópolis</div>
								</div>
								<div>
									<div class="fleft">cidade / estado</div>
									<div class="fright">londrina - pr</div>
								</div>
								<div>
									<div class="fleft">cep</div>
									<div class="fright">39.495-398</div>
								</div>
								<div>
									<div class="fleft">telefone</div>
									<div class="fright">(43) 3847-4987</div>
								</div>
								<div>
									<div class="fleft">financiado por</div>
									<div class="fright">itaú unibanco sa</div>
								</div>																
							</div>						
						</div><!--Gravames-->
						
						<h3>
							<span class="ico icoDpvat"></span>
							<span class="nome">DPVAT - resumo de quitação</span>
						</h3>
						<div>
							<table class="dpvat">
								<tbody>
									<tr>
										<td><img src="img/icones/dpvat.png" /></td>
										<td>exercício <span>2012</span></td>
										<td>status<br />data de pagamento</td>
										<td>quitado<br />23/01/2013</td>
									</tr>
									<tr>
										<td><img src="img/icones/dpvat.png" /></td>
										<td>exercício <span>2012</span></td>
										<td>status<br />data de pagamento</td>
										<td>quitado<br />23/01/2013</td>
									</tr>
									<tr>
										<td><img src="img/icones/dpvat.png" /></td>
										<td>exercício <span>2012</span></td>
										<td>status<br />data de pagamento</td>
										<td>quitado<br />23/01/2013</td>
									</tr>
									<tr>
										<td><img src="img/icones/dpvat.png" /></td>
										<td>exercício <span>2012</span></td>
										<td>status<br />data de pagamento</td>
										<td>quitado<br />23/01/2013</td>
									</tr>
									<tr>
										<td><img src="img/icones/dpvat.png" /></td>
										<td>exercício <span>2012</span></td>
										<td>status<br />data de pagamento</td>
										<td>quitado<br />23/01/2013</td>
									</tr>
								</tbody>
							</table>							
						</div><!--DPVAT - resumo de quitação-->

						<h3>
							<span class="ico icoDuplicidade"></span>
							<span class="nome">Duplicidade de motor</span>
						</h3>
						<div>
							<table>
								<thead>
									<tr>
										<td>placa</td>
										<td>chassi</td>
										<td>motor</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>bck 1985</td>
										<td>93ybsr29kaj319966</td>
										<td>k4md694q020145</td>
									</tr>
									<tr>
										<td>bck 1985</td>
										<td>93ybsr29kaj319966</td>
										<td>k4md694q020145</td>
									</tr>
									<tr>
										<td>bck 1985</td>
										<td>93ybsr29kaj319966</td>
										<td>k4md694q020145</td>
									</tr>
									<tr>
										<td>bck 1985</td>
										<td>93ybsr29kaj319966</td>
										<td>k4md694q020145</td>
									</tr>
								</tbody>
							</table>							
						</div><!--Duplicidade de motor-->
						
						<h3>
							<span class="ico icoRecall"></span>
							<span class="nome">Recall</span>
						</h3>
						<div>
							<div class="detalhes" style="width:100%">
								<div>
									<div class="fleft">marca</div>
									<div class="fright">volkswagen do brasil ltda.</div>
								</div>
								<div>
									<div class="fleft">produto</div>
									<div class="fright">volkswagen-golf 1.6</div>
								</div>
								<div>
									<div class="fleft">data da divulgação</div>
									<div class="fright">01/09/2006</div>
								</div>
								<div>
									<div class="fleft">chassi de início</div>
									<div class="fright">92839747495</div>
								</div>
								<div>
									<div class="fleft">chassi final</div>
									<div class="fright">83983838474</div>
								</div>
								<div>
									<div class="fleft">ano de início</div>
									<div class="fright">2006</div>
								</div>
								<div>
									<div class="fleft">ano final</div>
									<div class="fright">2006</div>
								</div>
								<div>
									<div class="fleft">fonte</div>
									<div class="fright">jornal</div>
								</div>
								<div>
									<div class="fleft">número do procedimento</div>
									<div class="fright">939384.3834793/2006-12</div>
								</div>
								<div>
									<div class="fleft" style="width:100%;">comunicado:</div>
									<div id="scrollbar1">
										<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
										<div class="viewport">
											<div class="overview">
												<h3>Magnis dis parturient montes</h3>
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vitae velit at velit pretium sodales. Maecenas egestas imperdiet mauris, vel elementum turpis iaculis eu. Duis egestas augue quis ante ornare eu tincidunt magna interdum. Vestibulum posuere risus non ipsum sollicitudin quis viverra ante feugiat. Pellentesque non faucibus lorem. Nunc tincidunt diam nec risus ornare quis porttitor enim pretium. Ut adipiscing tempor massa, a ullamcorper massa bibendum at. Suspendisse potenti. In vestibulum enim orci, nec consequat turpis. Suspendisse sit amet tellus a quam volutpat porta. Mauris ornare augue ut diam tincidunt elementum. Vivamus commodo dapibus est, a gravida lorem pharetra eu. Maecenas ultrices cursus tellus sed congue. Cras nec nulla erat.</p>

												<p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Pellentesque eget mauris libero. Nulla sit amet felis in sem posuere laoreet ut quis elit. Aenean mauris massa, pretium non bibendum eget, elementum sed nibh. Nulla ac felis et purus adipiscing rutrum. Pellentesque a bibendum sapien. Vivamus erat quam, gravida sed ultricies ac, scelerisque sed velit. Integer mollis urna sit amet ligula aliquam ac sodales arcu euismod. Fusce fermentum augue in nulla cursus non fermentum lorem semper. Quisque eu auctor lacus. Donec justo justo, mollis vel tempor vitae, consequat eget velit.</p>

												<p>Vivamus sed tellus quis orci dignissim scelerisque nec vitae est. Duis et elit ipsum. Aliquam pharetra auctor felis tempus tempor. Vivamus turpis dui, sollicitudin eget rhoncus in, luctus vel felis. Curabitur ultricies dictum justo at luctus. Nullam et quam et massa eleifend sollicitudin. Nulla mauris purus, sagittis id egestas eu, pellentesque et mi. Donec bibendum cursus nisi eget consequat. Nunc sit amet commodo metus. Integer consectetur lacus ac libero adipiscing ut tristique est viverra. Maecenas quam nibh, molestie nec pretium interdum, porta vitae magna. Maecenas at ligula eget neque imperdiet faucibus malesuada sed ipsum. Nulla auctor ligula sed nisl adipiscing vulputate. Curabitur ut ligula sed velit pharetra fringilla. Cras eu luctus est. Aliquam ac urna dui, eu rhoncus nibh. Nam id leo nisi, vel viverra nunc. Duis egestas pellentesque lectus, a placerat dolor egestas in. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec vitae ipsum non est iaculis suscipit.</p>

												<h3>Adipiscing risus </h3>
												<p>Quisque vel felis ligula. Cras viverra sapien auctor ante porta a tincidunt quam pulvinar. Nunc facilisis, enim id volutpat sodales, leo ipsum accumsan diam, eu adipiscing risus nisi eu quam. Ut in tortor vitae elit condimentum posuere vel et erat. Duis at fringilla dolor. Vivamus sem tellus, porttitor non imperdiet et, rutrum id nisl. Nunc semper facilisis porta. Curabitur ornare metus nec sapien molestie in mattis lorem ullamcorper. Ut congue, purus ac suscipit suscipit, magna diam sodales metus, tincidunt imperdiet diam odio non diam. Ut mollis lobortis vulputate. Nam tortor tortor, dictum sit amet porttitor sit amet, faucibus eu sem. Curabitur aliquam nisl sed est semper a fringilla velit porta. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum in sapien id nulla volutpat sodales ac bibendum magna. Cras sollicitudin, massa at sodales sodales, lacus tortor vestibulum massa, eu consequat dui nulla et ipsum.</p>

												<p>Aliquam accumsan aliquam urna, id vulputate ante posuere eu. Nullam pretium convallis tincidunt. Duis vitae odio arcu, ut fringilla enim. Nam ante eros, vestibulum sit amet rhoncus et, vehicula quis tellus. Curabitur sed iaculis odio. Praesent vitae ligula id tortor ornare luctus. Integer placerat urna non ligula sollicitudin vestibulum. Nunc vestibulum auctor massa, at varius nibh scelerisque eget. Aliquam convallis, nunc non laoreet mollis, neque est mattis nisl, nec accumsan velit nunc ut arcu. Donec quis est mauris, eu auctor nulla. Fusce leo diam, tempus a varius sit amet, auctor ac metus. Nam turpis nulla, fermentum in rhoncus et, auctor id sem. Aliquam id libero eu neque elementum lobortis nec et odio.</p>
											</div>
										</div>
									</div>         
								</div>	
							</div>						
						</div><!--Recall-->
						
						<h3>
							<span class="ico icoLeilao"></span>
							<span class="nome">Leilão</span>
						</h3>
						<div>
							<div class="introLeilao">
								<div class="imagem"><img src="img/icones/alertaG.png" /></div>
								<div class="texto">
									<div class="resumo">resumo</div>
									<div class="maisResumo">
										<p>total de leilões: <span>1</span></p>
										<p>data do último leilão: <span>20/02/2009</span></p>
										<p>total de fotos: <span>4</span></p>
									</div>
								</div>
							</div>
							<table class="introducaoLeilao">
								<thead>
									<tr>
										<td>primeiro leilão</td>
										<td>20/02/2009 (4 anos e 3 anos)</td>
									</tr>
								</thead>
							</table>						
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/martelo.jpg"/>
										<p class="legenda">Leilão</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">leiloeiro</div>
										<div class="fright">luiz fernando de abreu sodré santoro</div>
									</div>
									<div>
										<div class="fleft">pátio</div>
										<div class="fright">sodré santoro</div>
									</div>
									<div>
										<div class="fleft">comitente</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">número do leilão</div>
										<div class="fright">92837474</div>
									</div>
									<div>
										<div class="fleft">número do veículo</div>
										<div class="fright">82837479338</div>
									</div>																	
								</div>		
							</div>
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/bola.jpg"/>
										<p class="legenda">Veículos</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">Placa</div>
										<div class="fright">nha8212</div>
									</div>
									<div>
										<div class="fleft">Chassi</div>
										<div class="fright">9waa01j8293je977s0</div>
									</div>
									<div>
										<div class="fleft">Renavam</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">Marca / Modelo</div>
										<div class="fright">vw / golf flash</div>
									</div>
									<div>
										<div class="fleft">ano / fabricação</div>
										<div class="fright">2006</div>
									</div>
									<div>
										<div class="fleft">ano modelo</div>
										<div class="fright">2006</div>
									</div>
									<div>
										<div class="fleft">cor</div>
										<div class="fright">preta</div>
									</div>
									<div>
										<div class="fleft">combustível</div>
										<div class="fright">gasolina</div>
									</div>
									<div>
										<div class="fleft">categoria</div>
										<div class="fright">automóvel</div>
									</div>
									<div>
										<div class="fleft">nr. motor</div>
										<div class="fright">bpa111846</div>
									</div>
									<div>
										<div class="fleft">nr. câmbio</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">nr. carroceria</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">nr. eixo traseiro</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">nr. eixos</div>
										<div class="fright">2</div>
									</div>
								</div>		
							</div>
							<div>
								<div class="img situacao"></div>
								<div class="detalhes">
									<div>
										<div class="fleft">veículo</div>
										<div class="fright">batido</div>
									</div>
									<div>
										<div class="fleft">chassi</div>
										<div class="fright">não informado</div>
									</div>																								
								</div>		
							</div>
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/foto.jpg"/>
										<p class="legenda">fotos</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft"><img src="img/icones/fotos.png" style="margin:0 10px 0 0" />clique sobre as fotos para ampliar</div>
									</div>
									<div>
										<img class="modal" src="img/carros/timeline.jpg" width="108" />
										<img class="modal" src="img/carros/timeline.jpg" width="108" />
										<img class="modal" src="img/carros/timeline.jpg" width="108" />										
									</div>																								
								</div>		
							</div>
						</div><!--Leilao-->
						
						<h3>
							<span class="ico icoProprietarios"></span>
							<span class="nome">Proprietários</span>
						</h3>
						<div>
							<div class="introLeilao">
								<div class="imagem"><img src="img/icones/alertaG.png" /></div>
								<div class="texto">
									<div class="resumo">resumo</div>
									<div class="maisResumo">
										<p>total de proprietários: <span>3</span></p>
										<p>mais proprietários chassi: <a href="javascript:void(0);">clique aqui</a></p>
									</div>
								</div>
							</div>						
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/proprietarios.jpg"/>
										<p class="legenda">proprietário em 2010</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">nome</div>
										<div class="fright">rivelino tavares orsini</div>
									</div>
									<div>
										<div class="fleft">tipo de pessoa</div>
										<div class="fright">física</div>
									</div>
									<div>
										<div class="fleft">cpf / cnpj</div>
										<div class="fright">00293848504873</div>
									</div>
									<div>
										<div class="fleft">endereço</div>
										<div class="fright">av ind olegário maciel, 1408</div>
									</div>
									<div>
										<div class="fleft">cidade / uf</div>
										<div class="fright">uba - mg</div>
									</div>
									<div>
										<div class="fleft">cep</div>
										<div class="fright">34.318-023</div>
									</div>
									<div>
										<div class="fleft">telefone</div>
										<div class="fright">(021) 2394-3937</div>
									</div>
									<div>
										<div class="fleft">data de aquisição</div>
										<div class="fright">21/03/2010</div>
									</div>
								</div>		
							</div>
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/proprietarios.jpg"/>
										<p class="legenda">proprietário em 2008</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">nome</div>
										<div class="fright">rivelino tavares orsini</div>
									</div>
									<div>
										<div class="fleft">tipo de pessoa</div>
										<div class="fright">física</div>
									</div>
									<div>
										<div class="fleft">cpf / cnpj</div>
										<div class="fright">00293848504873</div>
									</div>
									<div>
										<div class="fleft">endereço</div>
										<div class="fright">av ind olegário maciel, 1408</div>
									</div>
									<div>
										<div class="fleft">cidade / uf</div>
										<div class="fright">uba - mg</div>
									</div>
									<div>
										<div class="fleft">cep</div>
										<div class="fright">34.318-023</div>
									</div>
									<div>
										<div class="fleft">telefone</div>
										<div class="fright">(021) 2394-3937</div>
									</div>
									<div>
										<div class="fleft">data de aquisição</div>
										<div class="fright">21/03/2010</div>
									</div>
								</div>		
							</div>
						</div><!--Proprietarios-->
						
						<h3>
							<span class="ico icoDecodificacao"></span>
							<span class="nome">Decodificação de chassi</span>
						</h3>
						<div>
							<div class="introLeilao">
								<div class="imagem"><img src="img/icones/alertaG.png" /></div>
								<div class="texto">
									<div class="resumo">resumo</div>
									<div class="maisResumo">
										<p>total de proprietários: <span>3</span></p>
										<p>mais proprietários chassi: <a href="javascript:void(0);">clique aqui</a></p>
									</div>
								</div>
							</div>													
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/decodificacao.jpg"/>
										<p class="legenda">características</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">fabricante</div>
										<div class="fright">volkswagen</div>
									</div>
									<div>
										<div class="fleft">marca / modelo</div>
										<div class="fright">volksvagem / golf flash 1.6 total flex</div>
									</div>
									<div>
										<div class="fleft">ano de fabricação</div>
										<div class="fright">2006</div>
									</div>
									<div>
										<div class="fleft">ano do modelo</div>
										<div class="fright">2006</div>
									</div>
									<div>
										<div class="fleft">mês de produção</div>
										<div class="fright">não informado</div>
									</div>									
								</div>		
							</div>
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/decodificacao.jpg"/>
										<p class="legenda">mecânicos</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">transmissão</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">motor</div>
										<div class="fright">1.6</div>
									</div>
									<div>
										<div class="fleft">tipo de motor</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">tração</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">tipo de freio</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">combustível</div>
										<div class="fright">flexível alcool / gasolina</div>
									</div>
									<div>
										<div class="fleft">código fipe</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">portas</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">tipo de carroceria</div>
										<div class="fright">hatchback</div>
									</div>
									<div>
										<div class="fleft">quantidade de eixos</div>
										<div class="fright">2</div>
									</div>
									<div>
										<div class="fleft">distância entre eixos</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">comprimento</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">peso bruto</div>
										<div class="fright">não informado</div>
									</div>
									<div>
										<div class="fleft">região geográfica</div>
										<div class="fright">america do sul</div>
									</div>
									<div>
										<div class="fleft">país de origem</div>
										<div class="fright">brasil</div>
									</div>
								</div>		
							</div>
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<p class="legenda">características</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">localização da fábrica</div>
										<div class="fright">são josé dos pinhais - pr</div>
									</div>								
								</div>		
							</div>
							<table class="introducaoLeilao">
								<thead>
									<tr>
										<td>resultado</td>
										<td>chassi decodificado corretamente</td>
									</tr>
								</thead>
							</table>							
						</div><!--Decodificação de chassi-->
						
						<h3>
							<span class="ico icoConcorrentes"></span>
							<span class="nome">Concorrentes</span>
						</h3>
						<div>
							<table>
								<thead>
									<tr>
										<td>marca</td>
										<td>modelo</td>
										<td>ano do modelo</td>
										<td>preço</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>honda</td>
										<td>civic sedan ex 1.7 16v 130cv aut. 4p</td>
										<td>r$ 37.500,00</td>
										<td>r$ 37.500,00</td>
									</tr>
									<tr>
										<td>vw - volkswagen</td>
										<td>golf 2.0/2.0 mi flex comfortline / sport</td>
										<td>R$ 41.466,67</td>
										<td>R$ 41.466,67</td>
									</tr>
									<tr>
										<td>fiat</td>
										<td>doblo ex 1.3 fire 16v 80cv 4/5p</td>
										<td>r$ 44,847,00</td>
										<td>r$ 44,847,00</td>
									</tr>
									<tr>
										<td>toyota</td>
										<td>corolla xli 1.6 15v 110cv aut.</td>
										<td>R$ 31.566,43</td>
										<td>R$ 31.566,43</td>
									</tr>
								</tbody>
							</table>							
						</div><!--Concorrentes-->
						
						<h3>
							<span class="ico icoEspecificacoes"></span>
							<span class="nome">Especificações de fábrica do modelo</span>
						</h3>
						<div>
							<div class="detalhes" style="width:100%">
								<div>
									<div class="fleft">fabricante</div>
									<div class="fright">renaut</div>
								</div>
								<div>
									<div class="fleft">modelo</div>
									<div class="fright">sandero vibe 1.6 8v</div>
								</div>
								<div>
									<div class="fleft">ano</div>
									<div class="fright">2010</div>
								</div>
								<div>
									<div class="fleft">combustível</div>
									<div class="fright">flexível</div>
								</div>
								<div>
									<div class="fleft">configuração</div>
									<div class="fright">hatch</div>
								</div>
								<div>
									<div class="fleft">procedência</div>
									<div class="fright">nacional</div>
								</div>
								<div>
									<div class="fleft">peso</div>
									<div class="fright">1055 kg</div>
								</div>
								<div>
									<div class="fleft">cilindrada</div>
									<div class="fright">1598 cc</div>
								</div>
								<div>
									<div class="fleft">porte</div>
									<div class="fright">pequeno</div>
								</div>
								<div>
									<div class="fleft">porta malas</div>
									<div class="fright">320 litros</div>
								</div>
								<div>
									<div class="fleft">tanque</div>
									<div class="fright">50 litros</div>
								</div>
								<div>
									<div class="fleft">comprimento</div>
									<div class="fright">4020 mm</div>
								</div>
								<div>
									<div class="fleft">entre eixos</div>
									<div class="fright">2590 mm</div>
								</div>
								<div>
									<div class="fleft">largura</div>
									<div class="fright">1750 mm</div>
								</div>
								<div>
									<div class="fleft">altura</div>
									<div class="fright">1530 mm</div>
								</div>
								<div>
									<div class="fleft">consumo urbano</div>
									<div class="fright">6,9 km/l</div>
								</div>
								<div>
									<div class="fleft">consumo rodoviário</div>
									<div class="fright">8,9 km/l</div>
								</div>
							</div>						
						</div><!--Especificações de fábrica do modelo-->
						
						<h3>
							<span class="ico icoDesvalorizacao"></span>
							<span class="nome">Desvalorização média</span>
						</h3>
						<div>
							<table class="valorDesvalorizacao">
								<thead>
									<tr>
										<td>2006 <p class="valorMedio">r$ 30.000,00</p></td>
										<td>2007 <p class="valorMedio">r$ 27.000,00</p></td>
										<td>2008 <p class="valorMedio">r$ 25.400,00</p></td>
										<td>2009 <p class="valorMedio">r$ 25.400,00</p></td>
										<td>2010 <p class="valorMedio">r$ 25.400,00</p></td>
										<td>2011 <p class="valorMedio">r$ 25.400,00</p></td>
										<td>2012 <p class="valorMedio">r$ 25.400,00</p></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>0km</td>
										<td>
											<div class="bloco laranja"><span class="desce"></span> -10%</div>
											<div class="bloco amarelo"><span class="desce"></span> -----</div>
										</td>
										<td>
											<div class="bloco laranja"><span class="desce"></span> -10%</div>
											<div class="bloco amarelo"><span class="desce"></span> -12%</div>
										</td>
										<td>
											<div class="bloco laranja"><span class="desce"></span> -10%</div>
											<div class="bloco amarelo"><span class="desce"></span> -12%</div>
										</td>
										<td>
											<div class="bloco laranja"><span class="desce"></span> -10%</div>
											<div class="bloco amarelo"><span class="desce"></span> -12%</div>
										</td>
										<td>
											<div class="bloco laranja"><span class="desce"></span> -10%</div>
											<div class="bloco amarelo"><span class="desce"></span> -12%</div>
										</td>
										<td>
											<div class="bloco laranja"><span class="desce"></span> -10%</div>
											<div class="bloco amarelo"><span class="desce"></span> -12%</div>
										</td>
									</tr>
								</tbody>
							</table>							
						</div><!--Desvalorização média-->
						
						<h3>
							<span class="ico icoRenajud"></span>
							<span class="nome">Renajud</span>
						</h3>
						<div>
							<div class="introLeilao">
								<div class="imagem"><img src="img/icones/alertaG.png" /></div>
								<div class="texto">
									<div class="resumo">resumo</div>
									<div class="maisResumo">
										<p>veículo com indicador de restrição judicial</p>
									</div>
								</div>
							</div>						
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/martelo.jpg"/>
										<p class="legenda">ocorrência</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">placa</div>
										<div class="fright">bks 9274</div>
									</div>
									<div>
										<div class="fleft">renavam</div>
										<div class="fright">169712516</div>
									</div>
									<div>
										<div class="fleft">marca</div>
										<div class="fright">fiat</div>
									</div>
									<div>
										<div class="fleft">modelo</div>
										<div class="fright">Fiat Adventure 1.8 16V</div>
									</div>
									<div>
										<div class="fleft">ano de fabricação</div>
										<div class="fright">2008</div>
									</div>
									<div>
										<div class="fleft">ano modelo</div>
										<div class="fright">2008</div>
									</div>
									<div>
										<div class="fleft">tribunal</div>
										<div class="fright">tribunal regional do trabalho 15 região</div>
									</div>
									<div>
										<div class="fleft">orgão judiciário</div>
										<div class="fright">vt p. prudente</div>
									</div>
									<div>
										<div class="fleft">processo</div>
										<div class="fright">187 1837-18-2000</div>
									</div>
									<div>
										<div class="fleft">restrições</div>
										<div class="fright">---</div>
									</div>
								</div>		
							</div>
							<div>
								<div class="img maisImg">
									<div class="posicao">
										<img class="imagem" src="img/icones/detalhamento/martelo.jpg"/>
										<p class="legenda">ocorrência</p>
									</div>
								</div>
								<div class="detalhes">
									<div>
										<div class="fleft">placa</div>
										<div class="fright">bks 9274</div>
									</div>
									<div>
										<div class="fleft">renavam</div>
										<div class="fright">169712516</div>
									</div>
									<div>
										<div class="fleft">marca</div>
										<div class="fright">fiat</div>
									</div>
									<div>
										<div class="fleft">modelo</div>
										<div class="fright">Fiat Adventure 1.8 16V</div>
									</div>
									<div>
										<div class="fleft">ano de fabricação</div>
										<div class="fright">2008</div>
									</div>
									<div>
										<div class="fleft">ano modelo</div>
										<div class="fright">2008</div>
									</div>
									<div>
										<div class="fleft">tribunal</div>
										<div class="fright">tribunal regional do trabalho 15 região</div>
									</div>
									<div>
										<div class="fleft">orgão judiciário</div>
										<div class="fright">vt p. prudente</div>
									</div>
									<div>
										<div class="fleft">processo</div>
										<div class="fright">187 1837-18-2000</div>
									</div>
									<div>
										<div class="fleft">restrições</div>
										<div class="fright">---</div>
									</div>
								</div>		
							</div>
						</div><!--Renajud-->
						
						<h3>
							<span class="ico icoSinistro"></span>
							<span class="nome">Sinistro</span>
						</h3>
						<div>
							<table>
								<thead>
									<tr>
										<td>data</td>
										<td>placa</td>
										<td>chassi</td>
										<td>nome</td>
										<td>tipo</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>15/06/2010</td>
										<td>zyz 8746</td>
										<td>93ybsr29kaj319966</td>
										<td>perda total</td>
										<td>PT</td>
									</tr>
									<tr>
										<td>12/08/12012</td>
										<td>ohy 3876</td>
										<td>93ybsr29kaj319966</td>
										<td></td>
										<td></td>
									</tr>
								</tbody>
							</table>							
						</div><!--Sinistro-->
						
						<h3>
							<span class="ico icoObservacoes"></span>
							<span class="nome">Observações</span>
						</h3>
						<div>
							<ul class="observacoes">
								<li><span class="dot">•</span> O sistema Carcheck depende de várias bases de dados para trazer precisão e confiabilidade à suas consultas.</li>
								<li><span class="dot">•</span> Ainda que empregando os melhores esforços, o Carcheck se exime de qualquer responsabildade pela eventual não inclusão de algum dado do veículo em razão de atraso ou falta de encaminhamento dos dados em qualquer uma das consultas oferecidas.</li>
								<li><span class="dot">•</span> O Carcheck não garante a procedência do veículo e que as bases consultadas possuam 100% das informações cadastradas, se limitando a apresentar as informações obtidas nas bases consultadas.</li>
							</ul>
						</div><!--Observações-->
						
						<h3>
							<span class="ico icoFeedback"></span>
							<span class="nome">Feedback</span>
						</h3>
						<div>
							<div class="valorExpectativas">
								<div class="tipo">Esta consulta atendeu as suas expectativas?</div>
								<div class="escolha"><input type="radio" name="expectativas" value="sim">Sim</div>
								<div class="escolha"><input type="radio" name="expectativas" value="nao">Não</div>
								<input type="submit" value="Enviar  &rsaquo;">
							</div>
						</div><!--Feedback-->

					</div><!--#detalhamento-->
				
				</div><!--.resultado-->

			</div><!--.wrap-->
		</div><!--.content-->		
		
		<div class="footer">
			<?php require_once('menuConsultas.php'); ?>
		</div><!--footer-->
		
		
<?php require_once('footer.php'); ?>